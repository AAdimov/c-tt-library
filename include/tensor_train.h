#pragma once
#include "tensor.h"
#include "parallel_cross_omp.h"

struct TTensorTrainParameters{
  public:
    double tolerance;
    int maximal_iterations_number, stop_rank, cross_max_iterations;
};

class TTensorTrain: public TTensor{
  protected:
    int *ranks;
    double **carriages;
  public:
    friend class TT;
    double operator[](const int *) const;
//double operator[](const int &);

    TTensorTrain operator*(const TTensorTrain &) const;
    TTensorTrain operator*(double) const;
    friend TTensorTrain operator*(double alpha, const TTensorTrain &tt){
      return tt * alpha;
    }
	  TTensorTrain operator+(const TTensorTrain &) const;
	  TTensorTrain operator-(const TTensorTrain &) const;
	  TTensorTrain & operator=(const TTensorTrain &);
	  void Approximate(TTensor *tensor, const TTensorTrainParameters &);
    TTensorTrain ElementwiseProduct(const TTensorTrain &, const TTensorTrainParameters &, const int if_compress,
                                     const int cutrank, const double eps_cut);
    int GetRank(int &) const;
    void SaveToFile(char *);
    void LoadFromFile(char *);
    //void Orthogonalize();
    //void Compress(const double &);
    double Nrm2() const;
    double Dot(const TTensorTrain &) const;
    TTensorTrain();
    TTensorTrain(const TTensorTrain &, const double &);
    TTensorTrain(const TTensorTrain &tens, const double *c);
    ~TTensorTrain();
    void SVDCompress(const double = 0.0, const int = 0);
};

class MulTTensor: public TTensor{
  private:
    const TTensorTrain *tt_one, *tt_two;
  public:
    double operator[](const int *) const;
    MulTTensor();
    MulTTensor(const int &d, int *&m);
    void SetTrains(const TTensorTrain *, const TTensorTrain *);
};
