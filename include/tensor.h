#pragma once
#include "matrix.h"

class TTensor{
  protected:
    int dimensionality;
    int *modes_sizes;
    TTensor();
    TTensor(const TTensor &);
  public:
    TTensor(const int, const int*);
    TTensor(const int, int*);
    virtual ~TTensor();
    virtual double operator[](const int *) const = 0;
    int GetDimensionality() const;
    int GetModeSize(const int &) const;
    int GetUnfoldingMatrixRowsNumber(const int &) const;
    int GetUnfoldingMatrixColumnsNumber(const int &) const;
};

class TUnfoldingSubMatrix: public TMatrix{
  private:
    TTensor *tensor;
    int k;
    int **row_indeces, **column_indeces;
  public:
    double Value(const int &, const int &);
    TUnfoldingSubMatrix(TTensor *, int, int, int, int **, int **);
    ~TUnfoldingSubMatrix();
};
