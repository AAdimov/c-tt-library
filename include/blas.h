#pragma once
#include <omp.h>
#include <complex>
#include <algorithm>
#include "mkl.h"
#include "mkl_lapacke.h"

//Int
void copy(const int &, const int *, const int &, int *, const int &);

//Double
void orthofactors(const int &, const int &, double * &, double * &, double * &);

void myreducedorthofactors(const double &, const int &, const int &, double *, double *, double *, int &, const int = 0);

void reducedorthofactors(const double &, const int &, const int &, double * &, double * &, double * &, int &);

void my_qr(int m, int n, double *a, int lda, double *matrR, int lda_r);

int gesvd(const int &, const char &, const char &, const lapack_int &, const lapack_int &, double* &, const lapack_int &,
          double* &, double* &, const lapack_int &, double* &, const lapack_int &, double* &);

void copy(const int &, const double *, const int &, double *, const int &);

void scal(const int &, const double &, double *, const int &);

void axpy(const int &, const double &, const double *, const int &, double *, const int &);

double nrm2(const int &, const double *, const int &);

void gemv(const CBLAS_ORDER, const CBLAS_TRANSPOSE, const int, const int, const double, const double *, const int,
          const double *, const int, const double, double *, const int);

void gemm(const CBLAS_ORDER, const CBLAS_TRANSPOSE, const CBLAS_TRANSPOSE, const int, const int, const int,
          const double, const double *, const int, const double *, const int, const double, double *, const int);

void trmm(const CBLAS_LAYOUT, const CBLAS_SIDE, const CBLAS_UPLO, const CBLAS_TRANSPOSE, const CBLAS_DIAG,
          const int, const int, const double, const double *, const int, double *, const int);

double dot_u(const int &, const double *, const int &, const double *, const int &);

double dot_c(const int &, const double *, const int &, const double *, const int &);

double * pseudoinverse(double, int, int, double *);

double imag(const double &);

double real(const double &);

int iamax(const int, const double *, const int);

int gesv(int, int, int, double*, int, int*, double*, int);

int gels(const int &, const char &, const lapack_int &, const lapack_int &, const lapack_int &,
        double*, const lapack_int &, double*, const lapack_int &);

int dsyevr(int, char, char, char, lapack_int, double*, lapack_int, double,
          double, lapack_int, lapack_int, double, lapack_int*, double*, double*, lapack_int, lapack_int*);

//int dsyevr(char, char, char, int, double *, int, double, double, int, int, double, int , double *, double *, int, int *,
//       double *, int, int *, int, int);

void ger(const CBLAS_ORDER, const int, const int, const double, const double *, const int,
        const double *, const int, double *, const int);

void transpose(const double *, const int, const int, double *, int = 0, int = 0);
