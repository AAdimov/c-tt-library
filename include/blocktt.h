#pragma once
#include "tt.h"

class BlockTT: public TT{
    int mu; // index of super_core
    int block_size; // number of TT-vectors
    double *block; // rank[mu] * modes_sizes[mu] x rank[k+1] * block_size;
  public:
    BlockTT();
    BlockTT(const TT &, int, int);
    BlockTT(const int, int*, int*, int, int);
    ~BlockTT();
    TT operator()(int b);
    int GetBlockSize() const;
    void ShiftRight(const double eps, const int cut_rank);
    void ShiftLeft(const double eps, const int cut_rank);
    void ALSSweep(const TT &, BlockTT &, const double, const int);
};
