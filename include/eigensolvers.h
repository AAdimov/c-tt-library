#pragma once
#include "tt.h"
#include "blocktt.h"

double RayleighQuotient(const TT &, TT &);

TT RayleighQuotientGrad(TT &, TT &);

TT & PowerIterations(const TT &, TT &, const double, const int = 100, unsigned int = 100u);

//TT * PowerIterationsDeflation(const TT &, TT *, const int, double, const int = 100, unsigned int = 100u);

TT * BlockPowerIterations(const TT &, TT *, const int, const double, const int = 100, unsigned int = 100u);

TT & RayleighQuotientIterations(const TT &, TT &, const double, const int = 100, unsigned int = 100u, int = 10, int = 5);

TT & InverseIterations(const TT &, TT &, double, const double, const int = 100, unsigned int = 100u, int = 10, int = 5);

TT * InverseIterations(const TT &, TT *, const int, double*, const double, const int = 100, unsigned int = 100u, int = 10, int = 5);

BlockTT & ALS(const TT &, BlockTT &, const double, const int, unsigned int);
