#pragma once
#include <iostream>
#include "cross.h"
#include "maxvol.h"
#include "mkl_dfti.h"
#include "cstdio"

struct TCross_Parallel_v1_Parameters{
  double tolerance;
  int maximal_iterations_number, number_of_checked_elements, max_rank, rank_increase, stop_rank;
  bool memory_strategy, start_from_column;
  TCross_Parallel_v1_Parameters();
};

struct TCross_Parallel_v1_Work_Data{
  bool *I, *J;
  double *current_row, *current_column;
  int omp_column_threads_num, omp_row_threads_num, *omp_column_start, *omp_column_num, *omp_row_start, *omp_row_num;
  TVolume max_volume;
  double global_max;
  TMatrix *matrix;
  TDifferenceMatrix work_matrix;
  TCross_Parallel_v1_Parameters parameters;
  TCross_Parallel_v1_Work_Data(TMatrix *, TMatrix *);
  ~TCross_Parallel_v1_Work_Data();
};

class TCross_Parallel_v1: public TCross_Base<TCross_Parallel_v1_Work_Data, TCross_Parallel_v1_Parameters>{
  private:
    double *U, *V, *C, *hat_A_inv, *RT, tolerance, norm, *AR, *CAT;
    int *rows_numbers, *columns_numbers;
    void PrepareData(TCross_Parallel_v1_Work_Data &, const TCross_Parallel_v1_Parameters &);
    void SearchMaxVolume(TCross_Parallel_v1_Work_Data &);
    bool StoppingCriteria(TCross_Parallel_v1_Work_Data &);
    void UpdateCross(TCross_Parallel_v1_Work_Data &);
    void GetDiffColumn(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
    void GetDiffRow(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
  public:
  	double * Matvec(double *&x, const char &option='f');
  	double Value(const int &, const int &);
    int GetRowNumber(const int &) const;
    int GetColumnNumber(const int &) const;
    TCross_Parallel_v1();
    ~TCross_Parallel_v1();
    const double * ExportC() const;
    const double * ExportHatAInv() const;
    const double * ExportRT() const;
    const double * ExportCAT() const;
    const double * ExportAR() const;
  	const double * ExportU() const{ return U; }
  	const double * ExportV() const{ return V; }
};
