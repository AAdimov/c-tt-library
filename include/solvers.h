#pragma once
#include "tt.h"

void TTGMRES(const TT &, TT &, TT &, TT (*)(const TT&, const TT&, double, const int), double, const int, int = 10, int = 5);
