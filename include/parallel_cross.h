#pragma once
#include <iostream>
#include <stddef.h>
#include "mpi.h"
#include "cross.h"
#include "mkl_cdft.h"

struct Send_Info{
  int number;
  double volume;
};

struct TCross_Parallel_v1_Parameters{
  double tolerance;
  int maximal_iterations_number, number_of_checked_elements, max_rank, rank_increase;
  bool memory_strategy;
  MPI_Comm communicator;
  TCross_Parallel_v1_Parameters(): tolerance(((double) 0.0)), maximal_iterations_number(((int) 1)), number_of_checked_elements(((int) 1)),
                                    max_rank(((int) 1)), rank_increase(((int) 1)), memory_strategy(true), communicator(MPI_COMM_WORLD){
  }
};

struct TCross_Parallel_v1_Work_Data{
  bool *I, *J;
  double *current_row, *current_column, *vec;
  int omp_column_threads_num, omp_row_threads_num, *omp_column_start, *omp_column_num, *omp_row_start, *omp_row_num;
  TVolume max_volume;
  double global_max;
  TMatrix * matrix;
  TDifferenceMatrix work_matrix;
  TCross_Parallel_v1_Parameters  parameters;
  TCross_Parallel_v1_Work_Data(TMatrix *, TMatrix *);
  ~TCross_Parallel_v1_Work_Data();
};

class TCross_Parallel_v1: public TCross_Base<TCross_Parallel_v1_Work_Data, TCross_Parallel_v1_Parameters>{
  private:
    double *U, *V, *C, *hat_A_inv, *RT, tolerance, norm;
    int * rows_numbers, * columns_numbers;
    void PrepareData(TCross_Parallel_v1_Work_Data &, const TCross_Parallel_v1_Parameters &);
    void SearchMaxVolume(TCross_Parallel_v1_Work_Data &);
    bool StoppingCriteria(TCross_Parallel_v1_Work_Data &);
    void UpdateCross(TCross_Parallel_v1_Work_Data &);
    void GetDiffColumn(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
    void GettDiffRow(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
    int row_comm_size, column_comm_size, processor_rank, mpi_column_start, mpi_column_num, mpi_row_start, mpi_row_num, mpi_avg_column, mpi_avg_row;
    MPI_Comm row_comm, column_comm;
  public:
    double Value(const int &, const int &);
    int GetRowNumber(const int &) const;
    int GetColumnNumber(const int &) const;
    TCross_Parallel_v1();
    ~TCross_Parallel_v1();
    const double * ExportC(){ return C; }
    const double * ExportHatAInv(){ return hat_A_inv; }
    const double * ExportRT(){ return RT; }
  	double * MatVec(double *&);
};
