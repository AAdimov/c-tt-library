#pragma once
#include <cmath>
#include "matrix.h"

class TVolume{
  private:
    double volume;
    int *rows_positions, *columns_positions;
    int size;
  public:
    TVolume(const int &, const double &, int *&, int *&);
    TVolume(const TVolume &);
    TVolume(const int &);
    void Set(const double &, int *, int *);
    double GetVolume() const;
    int GetRowPosition() const;
    int GetColumnPosition() const;
    bool operator>(const TVolume &) const;
    bool operator==(const TVolume &) const;
    TVolume& operator=(const TVolume &);
    bool operator!=(const TVolume &) const;
    ~TVolume();
};

template <class TCross_Work_Data, class TCross_Parameters>
class TCross_Base: public TMatrix{
  protected:
    int rank;
    virtual void PrepareData(TCross_Work_Data &, const TCross_Parameters &) = 0;
    virtual void SearchMaxVolume(TCross_Work_Data &) = 0;
    virtual bool StoppingCriteria(TCross_Work_Data &) = 0;
    virtual void UpdateCross(TCross_Work_Data &) = 0;
  public:
    void Approximate(TMatrix *matrix, const TCross_Parameters &parameters){
      this->rows_number = matrix->GetRowsNumber();
      this->columns_number = matrix->GetColumnsNumber();
      TCross_Work_Data WorkData(matrix, this);
      PrepareData(WorkData, parameters);
      while (true){
        SearchMaxVolume(WorkData);
        bool b = StoppingCriteria(WorkData);
        if (!b) UpdateCross(WorkData);
        else break;
      }
    }
    TCross_Base(): TMatrix(0, 0), rank(0){
    }
    int GetRank() const{
      return rank;
    }
};

class TMax_Volumes_Array{
  private:
    TVolume *volumes;
    int number_of_volumes;
  public:
    int GetNumberOfVolumes();
    const TVolume &GetVolume(const int &);
    const TVolume &operator[](const int &);
    void Insert(const TVolume &);
};
