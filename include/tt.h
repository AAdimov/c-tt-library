#pragma once
#include "tensor_train.h"

class TT: public TTensor{
  protected:
    unsigned int *ref_counter;
    int orthogonality; ///<  mu-orthogonality, -1 not, 0 == RightToLeft, kD - 1 == LeftToRight
    int power_scale;   ///<  2 ^ power_scale
    int max_rank; ///< maximal rank of carriages
    int max_carriages_size; ///< max(ranks[k] x modes_sizes[k] x ranks[k+1])
    int max_modes_size;
    int *ranks;
    double **carriages; ///< carriages[i] - ranks[k] x modes_sizes[k] x ranks[k+1] in fortran style.
  public:
    TT(); //< Default constructor
    TT(const int, int*, int*); // random carriages
    TT(const int, int*, int*, double**);
    TT(const TT &); ///< Copy constructor
    //TT(TT&&) noexcept; ///< Move constructor // need to implement for TTensor
    //TT& operator=(TT&&) noexcept; ///< Move-assignment operator // need to implement for TTensor
    virtual ~TT();
    TT(const TT &, const double &);
    void Copy(); // copy
    void Free(); // free

    double operator[](const int *) const; // need to check and not optimal
    TT & operator=(const TT &);  ///< Copy-assignment operator

    TT & operator+=(const TT &);
    TT & operator-=(const TT &);
    TT & operator*=(const TT &);
    TT & Apply_mu(const TT &, int);  ///< multiplication except mu-th core

    TT & operator*=(double);
    TT operator*(double) const;
    friend TT operator*(double alpha, const TT &tt){
      return tt * alpha;
    }
    TT operator+(const TT &) const;

    int GetRank(int &) const;
    int GetMaxRank() const;
    int GetPowerScale() const;
    void UpdateMax();

    double Nrm2(); // need to check
    double LogNorm();
    void Normalize();
    double Dot(const TT &) const; // need to check
    double * LeftToRightDot(const TT &a, const int mu) const;
    double * RightToLeftDot(const TT &a, const int mu) const;

    TT ConvertFromTTensorTrain(const TTensorTrain &); // copy
    TTensorTrain ConvertToTTensorTrain();             // copy
    /// Cross approximation.
    /** Detailed description. */
    void Approximate(TTensor *tt, const TTensorTrainParameters &);
    void LeftToRightOrthogonalization(const int mu); // need to check
    void LeftToRightOrthogonalization();
    void RightToLeftOrthogonalization(const int mu = 0); // need to check
    void LeftToRightCompression(const double eps, const int cut_rank, const int mu); // need to check
    void LeftToRightCompression(const double eps, const int cut_rank);
    void RightToLeftCompression(const double eps, const int cut_rank, const int mu = 0); // need to check
    void SVDCompression(const double = 0.0, const int = 0, const int mu = 0); // need to check
    void Orthogonalize(const int);
    double * MakeLocalSystem(TT &x, int mu, const double, int) const; ///< X_{!=mu}^T A X_{!=mu} r[mu]*n[mu]*r[mu+1] x r[mu]*n[mu]*r[mu+1]
    void Print(int mu = -1, int carriages = 0) const;
};

//TT operator+(TT lhs, const TT &rhs);
TT operator-(TT lhs, const TT &rhs);
TT operator*(TT lhs, const TT &rhs);

int Scale(int size, double *a);
int CutRank(int rank, const double eps, const int cut_rank, double *a);
