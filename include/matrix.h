#pragma once
#include <cstdlib>
#include "errors.h"
#include "blas.h"

class TMatrix{
  protected:
    int rows_number;
    int columns_number;
    TMatrix(const int &, const int &);
  public:
    virtual double Value(const int &, const int &) = 0;
    int GetRowsNumber() const;
    int GetColumnsNumber() const;
    virtual ~TMatrix();
};

class TSubMatrix: public TMatrix{
  private:
    int *row_indeces, *column_indeces;
    TMatrix *matrix;
  public:
    double Value(const int &, const int &);
    TSubMatrix(const int &, const int &, int *, int *, TMatrix *matr);
    ~TSubMatrix();
};

class TDifferenceMatrix: public TMatrix{
  private:
    TMatrix *Minuend_Matrix, *Subtrahend_Matrix;
  public:
    TDifferenceMatrix(TMatrix *, TMatrix *);
    double Value(const int &, const int &);
};
