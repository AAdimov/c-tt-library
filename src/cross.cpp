#include "../include/cross.h"

TVolume::TVolume(const int &s, const double &v, int * &i, int * &j):
  volume(v), size(s){
  rows_positions = (int *) malloc(size * sizeof(int));
  columns_positions = (int *) malloc(size * sizeof(int));
  copy(size, i, 1, rows_positions, 1);
  copy(size, j, 1, columns_positions, 1);
}

TVolume::TVolume(const int &s):
  size(s){
  rows_positions = (int *) calloc(size, sizeof(int));
  columns_positions = (int *) calloc(size, sizeof(int));
}

TVolume::~TVolume(){
  free(rows_positions);
  free(columns_positions);
}

void TVolume::Set(const double &v, int *i, int *j){
  copy(size, i, 1, rows_positions, 1);
  copy(size, j, 1, columns_positions, 1);
  volume = v;
}

double TVolume::GetVolume() const{
  return volume;
}

int TVolume::GetRowPosition() const{
  return rows_positions[0];
}

int TVolume::GetColumnPosition() const{
  return columns_positions[0];
}

bool TVolume::operator>(const TVolume & vol) const{
  return (fabs(volume) > fabs(vol.GetVolume())) ? true : false;
}

bool TVolume::operator==(const TVolume & vol) const{
  return ((fabs(fabs(volume) - fabs(vol.GetVolume())) <= 0) &&
      (rows_positions[0] == vol.GetRowPosition()) &&
      (columns_positions[0] == vol.GetColumnPosition())) ? true : false;
}

bool TVolume::operator!=(const TVolume & vol) const{
  return !((*this) == vol);
}

TVolume & TVolume::operator=(const TVolume & vol){
  volume = vol.GetVolume();
  rows_positions[0] = vol.GetRowPosition();
  columns_positions[0] = vol.GetColumnPosition();
  return *this;
}
