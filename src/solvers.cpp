#include "../include/solvers.h"

void TTGMRES(const TT &A, TT &x, TT &b, TT Mult(const TT&, const TT&, double, const int),
            double eps, const int cut_rank, int m, int max_restarts){
  double log_b_norm = b.LogNorm();
  double *H = (double *) malloc((m + 1) * m * sizeof(double));
  double *Hj = (double *) malloc((m + 1) * m * sizeof(double));
  double *beta_e = (double *) malloc((m + 1) * sizeof(double));
  TT *v = new TT[m + 1]();

  bool do_restart = true;
  int restarts = 0;
  while (do_restart){
    double zero = 0.0;
    copy((m + 1) * m, &zero, 0, H, 1);
    v[0] = b - Mult(A, x, eps, cut_rank);
    double log_beta = v[0].LogNorm();
    double log_curr_beta = log_beta;
    v[0].Normalize();
    int j = 0;
    for (; (j < m) && (log_curr_beta - log_b_norm > log(eps)/log(2)); ++j){
      double delta = std::min(1.0, pow(2, log(eps)/log(2) - log_curr_beta + log_beta));
      TT w = Mult(A, v[j], delta, cut_rank);
      for (int i = 0; i <= j; ++i){
        H[j * (m + 1) + i] = w.Dot(v[i]);
        w -= H[j * (m + 1) + i] * v[i];
        if (w.GetMaxRank() > cut_rank)
          w.SVDCompression(delta, cut_rank);
      }
      w.SVDCompression(delta, cut_rank);
      H[j * (m + 1) + j + 1] = pow(2, w.LogNorm());
      w.Normalize();
      v[j + 1] = w;
      copy(j + 2, &zero, 0, beta_e, 1);
      beta_e[0] = pow(2, log_beta);
      for (int i = 0; i <= j; ++i)
        copy(j + 2, H + i * (m + 1), 1, Hj + i * (j + 2), 1);
      gels(LAPACK_COL_MAJOR, 'N', j + 2, j + 1, 1, Hj, j + 2, beta_e, j + 2);
      log_curr_beta = log(abs(beta_e[j + 1]))/log(2);
    }
    for (int i = 0; i < j; ++i){
      x += v[i] * beta_e[i];
      if (x.GetMaxRank() > cut_rank)
        x.SVDCompression(eps, cut_rank);
    }
    x.SVDCompression(eps, cut_rank);
    ++restarts;
    do_restart = (log_curr_beta - log_b_norm > log(eps)/log(2)) && (restarts < max_restarts);
  }
  delete [] v;
  free(beta_e);
  free(Hj);
  free(H);
}
