#include "../include/parallel_cross_omp.h"
#include <cassert>

TCross_Parallel_v1_Parameters::TCross_Parallel_v1_Parameters():
  tolerance(0.0),
  maximal_iterations_number(1),
  number_of_checked_elements(1),
  max_rank(1),
  rank_increase(1),
  stop_rank(0),
  memory_strategy(true),
  start_from_column(true){
}

const double * TCross_Parallel_v1::ExportC() const{
  return C;
}

const double * TCross_Parallel_v1::ExportCAT() const{
  return CAT;
}

const double * TCross_Parallel_v1::ExportAR() const{
  return AR;
}

const double * TCross_Parallel_v1::ExportHatAInv() const{
  return hat_A_inv;
}

const double * TCross_Parallel_v1::ExportRT() const{
  return RT;
}

using namespace std;

void TCross_Parallel_v1::GetDiffColumn(const int &j, const TCross_Parallel_v1_Work_Data &work_data, double *&result){
  #pragma omp parallel for
  for (int i = 0; i < this->rows_number; ++i)
    if (!work_data.I[i])
      result[i] = work_data.matrix->Value(i, j);
//    copy(this->GetRowsNumber(), result, 1, C + this->rank*this->GetRowsNumber(), 1);
  gemv(CblasColMajor, CblasNoTrans, this->GetRowsNumber(), this->rank, - 1.0, U, this->GetRowsNumber(), V + j, this->GetColumnsNumber(), 1.0, result, 1);
  return;
}

void TCross_Parallel_v1::GetDiffRow(const int &i, const TCross_Parallel_v1_Work_Data &work_data, double *&result){
  #pragma omp parallel for
  for (int j = 0; j < this->columns_number; ++j)
    if (!work_data.J[j])
      result[j] = work_data.matrix->Value(i, j);
//    copy(this->GetColumnsNumber(), result, 1, RT + this->rank*this->GetColumnsNumber(), 1);
  gemv(CblasColMajor, CblasNoTrans, this->GetColumnsNumber(), this->rank, - 1.0, V, this->GetColumnsNumber(), U + i, this->GetRowsNumber(), 1.0, result, 1);
  return;
}

TCross_Parallel_v1_Work_Data::TCross_Parallel_v1_Work_Data(TMatrix *original_matrix, TMatrix *approximated_matrix):
  I(NULL),
  J(NULL),
  current_row(NULL),
  current_column(NULL),
  omp_column_threads_num(0),
  omp_row_threads_num(0),
  omp_column_start(NULL),
  omp_column_num(NULL),
  omp_row_start(NULL),
  omp_row_num(NULL),
  max_volume(1),
  global_max(0.0),
  matrix(original_matrix),
  work_matrix(original_matrix, approximated_matrix){
  I = (bool *) malloc(original_matrix->GetRowsNumber()*sizeof(bool));
  #pragma omp parallel for
  for (int i = ((int) 0); i < original_matrix->GetRowsNumber(); ++i)
    I[i] = false;
  J = (bool *) malloc(original_matrix->GetColumnsNumber()*sizeof(bool));
  #pragma omp parallel for
  for (int j = ((int) 0); j < original_matrix->GetColumnsNumber(); ++j)
    J[j] = false;
  omp_column_threads_num = omp_get_max_threads();
  omp_row_threads_num = omp_column_threads_num;
  int avg_column = (original_matrix->GetRowsNumber() - 1) / omp_column_threads_num + 1;
  int avg_row = (original_matrix->GetColumnsNumber() - 1) / omp_row_threads_num + 1;
  omp_column_threads_num = (original_matrix->GetRowsNumber() - 1) / avg_column + 1;
  omp_row_threads_num = (original_matrix->GetColumnsNumber() - 1) / avg_row + 1;
  omp_column_start = (int *) malloc(omp_column_threads_num * sizeof(int));
  omp_column_num = (int *) malloc(omp_column_threads_num * sizeof(int));
  omp_row_start = (int *) malloc(omp_row_threads_num * sizeof(int));
  omp_row_num = (int *) malloc(omp_row_threads_num * sizeof(int));
  omp_column_start[((int) 0)] = ((int) 0);
  omp_row_start[((int) 0)] = ((int) 0);
  for (int i = 1; i < omp_column_threads_num; ++i){
    omp_column_start[i] = omp_column_start[i - 1] + avg_column;
    omp_column_num[i - 1] = avg_column;
  }
  omp_column_num[omp_column_threads_num - 1] = original_matrix->GetRowsNumber() - omp_column_start[omp_column_threads_num - 1];
  for (int i = 1; i < omp_row_threads_num; ++i){
    omp_row_start[i] = omp_row_start[i - 1] + avg_row;
    omp_row_num[i - 1] = avg_row;
  }
  omp_row_num[omp_row_threads_num - 1] = original_matrix->GetColumnsNumber() - omp_row_start[omp_row_threads_num - 1];
}

TCross_Parallel_v1_Work_Data::~TCross_Parallel_v1_Work_Data(){
  free(I);
  free(J);
  free(current_row);
  free(current_column);
  free(omp_column_start);
  free(omp_column_num);
  free(omp_row_start);
  free(omp_row_num);
}

void TCross_Parallel_v1::PrepareData(TCross_Parallel_v1_Work_Data &work_data, const TCross_Parallel_v1_Parameters &parameters){
  norm = 0.0;
  free(U);
  free(V);
  free(C);
  free(hat_A_inv);
  free(RT);
  free(CAT);
  free(AR);
  U = NULL;
  V = NULL;
  C = NULL;
  CAT = NULL;
  AR = NULL;
  hat_A_inv = NULL;
  RT = NULL;
  free(rows_numbers);
  free(columns_numbers);
  rows_numbers = NULL;
  columns_numbers = NULL;
  tolerance = parameters.tolerance;
  work_data.parameters = parameters;
  if (work_data.parameters.max_rank <= ((int) 0))
    work_data.parameters.max_rank = 1;
  if ((work_data.parameters.rank_increase) <= ((int) 0) && (work_data.parameters.memory_strategy))
    work_data.parameters.rank_increase = 1;
  else if ((work_data.parameters.rank_increase) <= 1 && (!work_data.parameters.memory_strategy))
    work_data.parameters.rank_increase = 1 + 1;
  #pragma omp parallel sections
  {
    #pragma omp section
      U = (double *) malloc(work_data.parameters.max_rank * this->GetRowsNumber() * sizeof(double));
    #pragma omp section
      V = (double *) malloc(work_data.parameters.max_rank * this->GetColumnsNumber() * sizeof(double));
    #pragma omp section
      rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank * sizeof(int));
    #pragma omp section
      columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank * sizeof(int));
  }
  this->rank = ((int) 0);
}

TCross_Parallel_v1::TCross_Parallel_v1(): TCross_Base<TCross_Parallel_v1_Work_Data, TCross_Parallel_v1_Parameters>(),
  U(NULL),
  V(NULL),
  C(NULL),
  hat_A_inv(NULL),
  RT(NULL),
  tolerance(0.0),
  norm(0.0),
  AR(NULL),
  CAT(NULL),
  rows_numbers(NULL),
  columns_numbers(NULL){
    this->rank = ((int) 0);
}

TCross_Parallel_v1::~TCross_Parallel_v1(){
  free(U);
  free(V);
  free(C);
  free(hat_A_inv);
  free(RT);
  free(CAT);
  free(AR);
  free(rows_numbers);
  free(columns_numbers);
}

double TCross_Parallel_v1::Value(const int &i, const int &j){
  return dot_u(this->rank, U + i, this->GetRowsNumber(), V + j, this->GetColumnsNumber());
}

void TCross_Parallel_v1::SearchMaxVolume(TCross_Parallel_v1_Work_Data &work_data){
  if (this->rank == min(this->GetRowsNumber(), this->GetColumnsNumber())) return;
  if (this->rank >= work_data.parameters.max_rank){
    if (work_data.parameters.memory_strategy)
      work_data.parameters.max_rank += work_data.parameters.rank_increase;
    else
      work_data.parameters.max_rank *= work_data.parameters.rank_increase;
    #pragma omp parallel sections
    {
      #pragma omp section
        U = (double *) realloc(U, work_data.parameters.max_rank * this->GetRowsNumber() * sizeof(double));
      #pragma omp section
        V = (double *) realloc(V, work_data.parameters.max_rank * this->GetColumnsNumber() * sizeof(double));
      #pragma omp section
        rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
      #pragma omp section
        columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
    }
    #pragma omp barrier
  }
  work_data.current_column = U + this->rank * this->GetRowsNumber();
  work_data.current_row = V + this->rank * this->GetColumnsNumber();
  int ik = rand() % (this->GetRowsNumber() - this->rank), jk = rand() % (this->GetColumnsNumber() - this->rank);
  int i(0), j(0);
  int ia(0), ja(0);
  #pragma omp parallel sections
  {
    #pragma omp section
      while (ia <= ik){
        if (!work_data.I[i]) ++ia;
        ++i;
      }
    #pragma omp section
      while (ja <= jk){
        if (!work_data.J[j]) ++ja;
        ++j;
      }
  }
  i--;
  j--;
  #pragma omp barrier
  work_data.max_volume.Set(work_data.work_matrix.Value(i, j), &i, &j);
  int iters = ((int) 0), prev_column(j), prev_row(i);
  bool b;
  if (work_data.parameters.start_from_column){
    do{
      b = (work_data.max_volume.GetColumnPosition() == prev_column);
      if (iters == 0) b = false;
      if (!b){
        j = work_data.max_volume.GetColumnPosition();
        GetDiffColumn(j, work_data, work_data.current_column);
        i = work_data.max_volume.GetRowPosition();
        int ind[work_data.omp_column_threads_num];
        #pragma omp parallel for
        for (int o = ((int) 0); o < work_data.omp_column_threads_num; ++o){
          ind[o] = i;
          for (int k = work_data.omp_column_start[o]; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; ++k)
            if (!work_data.I[k])
              if (fabs(work_data.current_column[ind[o]]) < fabs(work_data.current_column[k]))
                ind[o] = k;
        }
        #pragma omp barrier
        i = ind[((int) 0)];
        for (int o = 1; o < work_data.omp_column_threads_num; ++o)
          if (fabs(work_data.current_column[i]) < fabs(work_data.current_column[ind[o]]))
            i = ind[o];
        work_data.max_volume.Set(work_data.current_column[i], &i, &j);
        prev_column = work_data.max_volume.GetColumnPosition();
      }
      b = (work_data.max_volume.GetRowPosition() == prev_row);
      if (iters == 0) b = false;
      if (!b){
        i = work_data.max_volume.GetRowPosition();
        GetDiffRow(i, work_data, work_data.current_row);
        j = work_data.max_volume.GetColumnPosition();
        int ind[work_data.omp_row_threads_num];
        #pragma omp parallel for
        for (int o = ((int) 0); o < work_data.omp_row_threads_num; ++o){
          ind[o] = j;
          for (int k = work_data.omp_row_start[o]; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; ++k)
            if (!work_data.J[k])
              if (fabs(work_data.current_row[ind[o]]) < fabs(work_data.current_row[k]))
                ind[o] = k;
        }
        #pragma omp barrier
        j = ind[((int) 0)];
        for (int o = 1; o < work_data.omp_row_threads_num; ++o)
          if (fabs(work_data.current_row[j]) < fabs(work_data.current_row[ind[o]]))
            j = ind[o];
        work_data.max_volume.Set(work_data.current_row[j], &i, &j);
        prev_row = work_data.max_volume.GetRowPosition();
      }
      ++iters;
    } while ((!b) || ((work_data.parameters.maximal_iterations_number > 0) && (iters < work_data.parameters.maximal_iterations_number)));
  }
  else{
    do{
      b = (work_data.max_volume.GetRowPosition() == prev_row);
      if (iters == 0) b = false;
      if (!b){
        i = work_data.max_volume.GetRowPosition();
        GetDiffRow(i, work_data, work_data.current_row);
        j = work_data.max_volume.GetColumnPosition();
        int ind[work_data.omp_row_threads_num];
        #pragma omp parallel for
        for (int o = ((int) 0); o < work_data.omp_row_threads_num; ++o){
          ind[o] = j;
          for (int k = work_data.omp_row_start[o]; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; ++k)
            if (!work_data.J[k])
              if (fabs(work_data.current_row[ind[o]]) < fabs(work_data.current_row[k]))
                ind[o] = k;
        }
        #pragma omp barrier
        j = ind[((int) 0)];
        for (int o = 1; o < work_data.omp_row_threads_num; ++o)
          if (fabs(work_data.current_row[j]) < fabs(work_data.current_row[ind[o]]))
            j = ind[o];
        work_data.max_volume.Set(work_data.current_row[j], &i, &j);
        prev_row = work_data.max_volume.GetRowPosition();
      }
      b = (work_data.max_volume.GetColumnPosition() == prev_column);
      if (iters == 0) b = false;
      if (!b){
        j = work_data.max_volume.GetColumnPosition();
        GetDiffColumn(j, work_data, work_data.current_column);
        i = work_data.max_volume.GetRowPosition();
        int ind[work_data.omp_column_threads_num];
        #pragma omp parallel for
        for (int o = ((int) 0); o < work_data.omp_column_threads_num; ++o){
          ind[o] = i;
          for (int k = work_data.omp_column_start[o]; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; ++k)
            if (!work_data.I[k])
              if (fabs(work_data.current_column[ind[o]]) < fabs(work_data.current_column[k]))
                ind[o] = k;
        }
        #pragma omp barrier
        i = ind[((int) 0)];
        for (int o = 1; o < work_data.omp_column_threads_num; ++o)
          if (fabs(work_data.current_column[i]) < fabs(work_data.current_column[ind[o]]))
            i = ind[o];
        work_data.max_volume.Set(work_data.current_column[i], &i, &j);
        prev_column = work_data.max_volume.GetColumnPosition();
      }
      ++iters;
    } while ((!b) || ((work_data.parameters.maximal_iterations_number > 0) && (iters < work_data.parameters.maximal_iterations_number)));
  }
  if (prev_column != work_data.max_volume.GetColumnPosition())
    GetDiffColumn(work_data.max_volume.GetColumnPosition(), work_data, work_data.current_column);
  if (prev_row != work_data.max_volume.GetRowPosition())
    GetDiffRow(work_data.max_volume.GetRowPosition(), work_data, work_data.current_row);
}

bool TCross_Parallel_v1::StoppingCriteria(TCross_Parallel_v1_Work_Data &work_data){
  if (fabs(work_data.max_volume.GetVolume()) > fabs(work_data.global_max))
    work_data.global_max = fabs(work_data.max_volume.GetVolume());
  if (((work_data.parameters.stop_rank > 0) && (this->rank >= work_data.parameters.stop_rank)) ||
      (sqrt(real(norm)) * fabs(tolerance) >= fabs(work_data.max_volume.GetVolume()) *
       sqrt(this->GetColumnsNumber()-this->rank) * sqrt(this->GetRowsNumber()-this->rank))){
    int *new_row_indeces, *new_column_indeces;
    #pragma omp parallel sections
    {
      #pragma omp section
        U = (double *) realloc(U, this->rank * this->GetRowsNumber() * sizeof(double));
      #pragma omp section
        V = (double *) realloc(V, this->rank * this->GetColumnsNumber() * sizeof(double));
      #pragma omp section
        C = (double *) malloc(this->rank * this->GetRowsNumber() * sizeof(double));
      #pragma omp section
        RT = (double *) malloc(this->rank * this->GetColumnsNumber() * sizeof(double));
      #pragma omp section
        rows_numbers = (int *) realloc(rows_numbers ,this->rank*sizeof(int));
      #pragma omp section
        columns_numbers = (int *) realloc(columns_numbers,this->rank*sizeof(int));
      #pragma omp section
        CAT = (double *) malloc(this->rank * this->GetRowsNumber() * sizeof(double));
      #pragma omp section
        AR = (double *) malloc(this->rank * this->GetColumnsNumber() * sizeof(double));
      #pragma omp section
        new_row_indeces = (int *) malloc(this->rank * sizeof(int));
      #pragma omp section
        new_column_indeces = (int *) malloc(this->rank * sizeof(int));
    }
    #pragma omp barrier
    MaxVol(rows_number, rank, U, rows_numbers, tolerance, CAT);
    for (int k = 0; k < rank; ++k)
      gemv(CblasColMajor, CblasNoTrans, GetColumnsNumber(), rank, 1.0, V, GetColumnsNumber(), U + rows_numbers[k], GetRowsNumber(), 0.0, RT + k * GetColumnsNumber(), 1);
    MaxVol(columns_number, rank, V, columns_numbers, tolerance, AR);
    for (int k = 0; k < rank; ++k)
      gemv(CblasColMajor, CblasNoTrans, GetRowsNumber(), rank, 1.0, U, GetRowsNumber(), V + columns_numbers[k], GetColumnsNumber(), 0.0, C + k * GetRowsNumber(), 1);
    int *tmp_ipiv;
    double *tmp_matrix;
    tmp_matrix = (double *) malloc(rank * rank * sizeof(double));
    tmp_ipiv = (int *) malloc(rank * sizeof(int));
    for (int i = 0; i < rank; ++i)
      copy(rows_number, C + i * rows_number, 1, CAT + i, rank);
    for (int i = 0; i < rank; ++i){
      double *pointer1 = C + rows_numbers[i];
      double *pointer2 = tmp_matrix + i * rank;
      copy(rank, pointer1, rows_number, pointer2, 1);
    }
    gesv(LAPACK_COL_MAJOR, rank, rows_number, tmp_matrix, rank, tmp_ipiv, CAT, rank);
    for (int i = 0; i < rank; ++i)
      copy(columns_number, RT + i * columns_number, 1, AR + i, rank);
    for (int i = 0; i < rank; ++i){
      double *pointer1 = RT + columns_numbers[i];
      double *pointer2 = tmp_matrix + i * rank;
      copy(rank, pointer1, columns_number, pointer2, 1);
    }
    gesv(LAPACK_COL_MAJOR, rank, columns_number, tmp_matrix, rank, tmp_ipiv, AR, rank);
    free(tmp_ipiv);
    free(tmp_matrix);
    double *hat_A = (double *) malloc(this->rank * this->rank * sizeof(double));
    #pragma omp parallel for
    for (int k = 0; k < this->rank; ++k)
      copy(this->rank, RT + columns_numbers[k], this->GetColumnsNumber(), hat_A + k * this->rank, 1);
    hat_A_inv = pseudoinverse(1e-8, this->rank, this->rank, hat_A);
    free(hat_A);
    work_data.current_row = NULL;
    work_data.current_column = NULL;
    return true;
  }
  else
    return false;
}

void TCross_Parallel_v1::UpdateCross(TCross_Parallel_v1_Work_Data &work_data){
    double column_factor(1.0/sqrt(fabs(work_data.max_volume.GetVolume()))), row_factor = 1.0 / (work_data.max_volume.GetVolume() * column_factor);
    #pragma omp parallel sections
    {
      #pragma omp section
      {
        #pragma omp parallel for
        for (int i = ((int) 0); i < this->GetRowsNumber(); ++i)
          if (!work_data.I[i])
            work_data.current_column[i] *= column_factor;
          else
            work_data.current_column[i] = 0.0;
      }
      #pragma omp section
      {
        #pragma omp parallel for
        for (int j = ((int) 0); j < this->GetColumnsNumber(); ++j)
          if (!work_data.J[j])
            work_data.current_row[j] *= row_factor;
          else
            work_data.current_row[j] = 0.0;
      }
    }
    work_data.I[work_data.max_volume.GetRowPosition()] = true;
    work_data.J[work_data.max_volume.GetColumnPosition()] = true;
    rows_numbers[this->rank] = work_data.max_volume.GetRowPosition();
    columns_numbers[this->rank] = work_data.max_volume.GetColumnPosition();
    double *b = (double *) malloc((this->rank + 1) * sizeof(double)), *x = (double *) malloc((this->rank + 1) * sizeof(double));
    gemv(CblasColMajor, CblasConjTrans, this->GetRowsNumber(), this->rank + 1, 1.0, U, this->GetRowsNumber(), work_data.current_column, 1, 0.0, b, 1);
    gemv(CblasColMajor, CblasConjTrans, this->GetColumnsNumber(), this->rank + 1, 1.0, V, this->GetColumnsNumber(), work_data.current_row, 1, 0.0, x, 1);
    norm += 2.0 * real(dot_c(this->rank, b, 1, x, 1)) + b[this->rank]*x[this->rank];
    free(x);
    free(b);
    ++this->rank;
    work_data.current_column = NULL;
    work_data.current_row = NULL;
}

int TCross_Parallel_v1::GetRowNumber(const int &k) const{
  return rows_numbers[k];
}

int TCross_Parallel_v1::GetColumnNumber(const int &k) const{
  return columns_numbers[k];
}
