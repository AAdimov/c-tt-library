#include "../include/eigensolvers.h"
#include "../include/solvers.h"

double RayleighQuotient(const TT &A, TT &x){
  x.Normalize();
	return ((A * x).Dot(x));
}

TT RayleighQuotientGrad(TT &tt, TT &x){
	return 2/(x.Dot(x)) * (tt * x - RayleighQuotient(tt, x) * x);
}

void Orthogonalization(TT *evs, int i, const double eps, const int cut_rank){
  for (int j = 0; j < i; ++j){
    evs[i] -= (evs[i].Dot(evs[j])) * evs[j];
    evs[i].SVDCompression(eps, cut_rank);
  }
  evs[i].Normalize();
}

TT & PowerIterations(const TT &tt, TT &ev, const double eps, const int cut_rank, unsigned int niters){
  ev.SVDCompression(eps, cut_rank);
  ev.Normalize();
  for (unsigned int iters = 0; iters < niters; ++iters){
    ev = tt * ev;
    ev.SVDCompression(eps, cut_rank);
    ev.Normalize();
  }
  return ev;
}
/*
TT * PowerIterationsDeflation(const TT &tt, TT *evs, const int B, double eps, const int cut_rank, unsigned int niters){
  evs[0] = PowerIterations(tt, evs[0], eps, cut_rank, niters);
  for (int i = 1; i < B; ++i){
    evs[i].SVDCompression(eps);
    evs[i].Normalize();
    Orthogonalization(evs, i, eps, cut_rank);
    for (unsigned int iters = 0; iters < niters; ++iters){
      evs[i] = tt * evs[i];
      evs[i].SVDCompression(eps, cut_rank);
      Orthogonalization(evs, i, eps, cut_rank);
    }
  }
  return evs;
}
*/
TT * BlockPowerIterations(const TT &tt, TT *evs, const int B, const double eps, const int cut_rank, unsigned int niters){
  for (int i = 0; i < B; ++i){
    evs[i].SVDCompression(eps, cut_rank);
    evs[i].Normalize();
  }
  for (unsigned int iters = 0; iters < niters; ++iters){
    for (int i = 0; i < B; ++i){
      evs[i] = tt * evs[i];
      evs[i].SVDCompression(eps, cut_rank);
      Orthogonalization(evs, i, eps, cut_rank);
    }
  }
  return evs;
}

TT Multiplication(const TT &A, const TT &x, const double eps, const int cut_rank){
  TT C = A * x;
  C.SVDCompression(eps, cut_rank);
  return C;
}

TT & RayleighQuotientIterations(const TT &tt, TT &ev, const double eps, const int cut_rank, unsigned int niters,
                      int m, int max_restarts){
  TT I(tt, 1.0);
  ev.Normalize();
  ev.SVDCompression(eps, cut_rank);
  for (unsigned int iters = 0; iters < niters; ++iters){
    TT tt_shifted = tt - RayleighQuotient(tt, ev) * I;
    TT rhs = ev;
    TTGMRES(tt_shifted, ev, rhs, Multiplication, eps, cut_rank, m, max_restarts);
    ev.Normalize();
  }
  return ev;
}

TT & InverseIterations(const TT &tt, TT &ev, double shift, const double eps, const int cut_rank, unsigned int niters,
                      int m, int max_restarts){
  TT I(tt, 1.0);
  TT tt_shifted = tt - shift * I;
  ev.Normalize();
  ev.SVDCompression(eps, cut_rank);
  for (unsigned int iters = 0; iters < niters; ++iters){
    TT rhs = ev;
    TTGMRES(tt_shifted, ev, rhs, Multiplication, eps, cut_rank, m, max_restarts);
    ev.Normalize();
  }
  return ev;
}

TT * InverseIterations(const TT &tt, TT *evs, const int B, double *shift, const double eps, const int cut_rank,
                      unsigned int niters, int m, int max_restarts){
  TT I(tt, 1.0);
  TT *tt_shifted = new TT[B]();
  for (int i = 0; i < B; ++i){
    tt_shifted[i] = tt - shift[i] * I;
    evs[i].SVDCompression(eps, cut_rank);
    evs[i].Normalize();
  }
  for (unsigned int iters = 0; iters < niters; ++iters){
    for (int i = 0; i < B; ++i){
      TT rhs = evs[i];
      TTGMRES(tt_shifted[i], evs[i], rhs, Multiplication, eps, cut_rank, m, max_restarts);
      Orthogonalization(evs, i, eps, cut_rank);
    }
  }
  delete [] tt_shifted;
  return evs;
}

BlockTT & ALS(const TT &tt, BlockTT &x, const double eps, const int cut_rank, unsigned int niters){
  for (unsigned int iters = 0; iters < niters; ++iters)
    x.ALSSweep(tt, x, eps, cut_rank);
  return x;
}

/*
void TTLanczos(const TT &A, TT &x, TT &b, TT Mult(const TT&, const TT&, double, const int),
            double eps, const int cut_rank, int m, int max_restarts){
}*/
