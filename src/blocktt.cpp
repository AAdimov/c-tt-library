#include "../include/blocktt.h"

BlockTT::BlockTT():
  TT(),
  mu(0),
  block_size(0),
  block(nullptr){
}

BlockTT::BlockTT(const TT &tt, int mu, int block_size):
  TT(tt),
  mu(mu),
  block_size(block_size),
  block(nullptr){
    //this->Copy();
    int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1] * block_size;
    this->block = (double *) malloc(size * sizeof(double));
    for (int i = 0; i < size; ++i)
      this->block[i] = rand() % 1000000000 / 100000000.0;
}

BlockTT::BlockTT(const int d, int *modes_sizes, int *tt_ranks, int mu, int block_size):
  TT(d, modes_sizes, tt_ranks),
  mu(mu),
  block_size(block_size),
  block(nullptr){
    int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1] * block_size;
    this->block = (double *) malloc(size * sizeof(double));
    for (int i = 0; i < size; ++i)
      this->block[i] = rand() % 1000000000 / 100000000.0;
}

BlockTT::~BlockTT(){
  free(block);
  block = nullptr;
}

TT BlockTT::operator()(int b){
  if ((b < 0) || (b > this->block_size)) return TT();
  int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1];
  this->carriages[mu] = (double *) realloc(this->carriages[mu], size * sizeof(double));
  copy(size, this->block + b * size, 1, this->carriages[mu], 1);
  TT result(this->dimensionality, this->modes_sizes, this->ranks, this->carriages);
  return result;
}

int BlockTT::GetBlockSize() const{
  return this->block_size;
}

void BlockTT::ShiftRight(const double eps, const int cut_rank){
  Orthogonalize(mu);
  if (mu == this->dimensionality - 1) return;
  // block = U * S * Block_VT
  const unsigned int k = mu;
  int m = this->ranks[k] * this->modes_sizes[k];  // number of rows
  int n = this->ranks[k + 1] * block_size; // number of columns
  int rank = std::min(m, n);
  double *U = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  double *tmp = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  double *Block_VT = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  double *sigma = (double *) malloc(rank * sizeof(double));
  double *superb = (double *) malloc((rank - 1) * sizeof(double));
  gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, this->block, m, sigma, U, m, tmp, rank, superb);
  int new_rank = CutRank(rank, eps, cut_rank, sigma);
  int scale = Scale(new_rank, sigma);
  this->power_scale += scale;
  double factor = pow(2, -scale);
  for (int i = 0; i < new_rank; ++i){
    scal(n, factor * sigma[i], tmp + i, rank);
    copy(n, tmp + i, rank, Block_VT + i, new_rank);
  }
  //this->carriages[k] = U;
  this->carriages[k] = (double *) realloc(this->carriages[k], m * new_rank * sizeof(double));
  copy(m * new_rank, U, 1, this->carriages[k], 1);
  int old_rank = this->ranks[k + 1];
  this->ranks[k + 1] = new_rank;
  // Block_VT * this->carriages[k + 1]^R, this->carriages[k + 1] = ranks[k + 1] x modes_sizes[k + 1] * ranks[k + 2]
  int size = new_rank * this->modes_sizes[k + 1] * this->ranks[k + 2];
  this->block = (double *) realloc(this->block, size * this->block_size * sizeof(double));
  for (int i = 0; i < this->block_size; ++i){
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, new_rank, this->modes_sizes[k + 1] * this->ranks[k + 2], old_rank,
        1.0, Block_VT + i * new_rank * old_rank, new_rank, this->carriages[k + 1], old_rank, 0.0,
        this->block + i * size, new_rank);
  }
  this->carriages[k + 1] = (double *) realloc(this->carriages[k + 1], size * sizeof(double));
  scale = Scale(size * block_size, this->block);
  this->power_scale += scale;
  scal(size * block_size, pow(2, -scale), this->block, 1);
  // Update max_rank and max_carriages_size
  this->UpdateMax();
  ++mu;
  this->orthogonality = mu;
  free(superb);
  free(sigma);
  free(Block_VT);
  free(tmp);
  free(U);
}

void BlockTT::ShiftLeft(const double eps, const int cut_rank){
  Orthogonalize(mu);
  if (mu == 0) return;
  double *buffer = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  //copy block to buffer = ranks[k] * block_size x modes_sizes[k] * ranks[k + 1]
  const unsigned int k = mu;
  for (int p = 0; p < block_size; ++p)
    for (int i = 0; i < this->modes_sizes[k]; ++i)
      for (int j = 0; j < this->ranks[k + 1]; ++j){
        double *from = block + ((p * this->ranks[k + 1] + j) * this->modes_sizes[k] + i) * this->ranks[k];
        double *to = buffer + ((j * this->modes_sizes[k] + i) * block_size + p) * this->ranks[k];
        copy(this->ranks[k], from, 1, to, 1);
      }
  // buffer = Block_U * S * VT
  // this->carriages[k] = ranks[k] x modes_sizes[k] * ranks[k + 1]
  int m = this->ranks[k] * block_size;  // number of rows
  int n = this->modes_sizes[k] * this->ranks[k + 1]; // number of columns
  int rank = std::min(m, n);
  double *Block_U = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  double *tmp = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  double *VT = (double *) malloc(this->max_carriages_size * block_size * sizeof(double));
  double *sigma = (double *) malloc(rank * sizeof(double));
  double *superb = (double *) malloc((rank - 1) * sizeof(double));
  gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, buffer, m, sigma, Block_U, m, VT, rank, superb);
  int new_rank = CutRank(rank, eps, cut_rank, sigma);
  this->carriages[k] = (double *) realloc(this->carriages[k], new_rank * n * sizeof(double));
  int scale = Scale(new_rank, sigma);
  this->power_scale += scale;
  double factor = pow(2, -scale);
  for (int i = 0; i < new_rank; ++i){
    scal(m, factor * sigma[i], Block_U + i * m, 1);
    copy(n, VT + i, rank, this->carriages[k] + i, new_rank); // this->carriages[k] = VT
  }
  // this->carriages[k - 1]^L * Block_U, this->carriages[k - 1] = ranks[k - 1] * modes_sizes[k - 1] x ranks[k]
  int size = this->ranks[k - 1] * this->modes_sizes[k - 1] * new_rank;
  this->block = (double *) realloc(this->block, size * block_size * sizeof(double));
  for (int i = 0; i < block_size; ++i){
    int lda = this->ranks[k - 1] * this->modes_sizes[k - 1];
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, lda, new_rank, this->ranks[k], 1.0,
      this->carriages[k - 1], lda, Block_U + i * this->ranks[k], m,
      0.0, this->block + i * size, lda);
  }
  this->carriages[k - 1] = (double *) realloc(this->carriages[k - 1], size * sizeof(double));
  scale = Scale(size * block_size, this->block);
  this->power_scale += scale;
  scal(size * block_size, pow(2, -scale), this->block, 1);
  this->ranks[k] = new_rank;
  // Update max_rank and max_carriages_size
  this->UpdateMax();
  --mu;
  this->orthogonality = mu;
  free(superb);
  free(sigma);
  free(VT);
  free(Block_U);
  free(tmp);
  free(buffer);
}

void BlockTT::ALSSweep(const TT &tt, BlockTT &x, const double eps, const int cut_rank){
  const int kD = x.GetDimensionality();
  Orthogonalize(mu);
  int m = x.block_size;
  int *isuppz = (int *) malloc(2 * m * sizeof(int));
  for (int k = 0; k < kD; ++k){ // left to right sweep
    double *A_mu = tt.MakeLocalSystem(x, k, eps, cut_rank);
    //x.block = solve();
    int n = x.ranks[k] * x.modes_sizes[k] * x.ranks[k + 1];
    double *evs = (double*) malloc(n * sizeof(double));
    int info = dsyevr(LAPACK_COL_MAJOR, 'V', 'I', 'L', n, A_mu, n, 0.0, 1.0, n - m + 1, n, 0.0, &m, evs, x.block, n, isuppz);
    free(evs);
    free(A_mu);
    x.ShiftRight(eps, cut_rank);
  }
  for (int k = kD - 1; k >= 0; --k){ // right to left sweep
    double *A_mu = tt.MakeLocalSystem(x, k, eps, cut_rank);
    int n = x.ranks[k] * x.modes_sizes[k] * x.ranks[k + 1];
    double *evs = (double*) malloc(n * sizeof(double));
    int info = dsyevr(LAPACK_COL_MAJOR, 'V', 'I', 'L', n, A_mu, n, 0.0, 1.0, n - m + 1, n, 0.0, &m, evs, x.block, n, isuppz);
    free(evs);
    free(A_mu);
    x.ShiftLeft(eps, cut_rank);
  }
  free(isuppz);
}
