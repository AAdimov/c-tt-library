#include "../include/matrix.h"
TMatrix::TMatrix(const int &m, const int &n):
  rows_number(m),
  columns_number(n){
}

TMatrix::~TMatrix(){
  rows_number = 0;
  columns_number = 0;
}

int TMatrix::GetRowsNumber() const{
  return rows_number;
}

int TMatrix::GetColumnsNumber() const{
  return columns_number;
}

TSubMatrix::TSubMatrix(const int &m, const int &n, int *r, int *c, TMatrix *matr):
  TMatrix(m, n),
  row_indeces(NULL),
  column_indeces(NULL){
  row_indeces = (int *) malloc(m * sizeof(int));
  copy(m, r, 1, row_indeces, 1);
  column_indeces = (int *) malloc(n * sizeof(int));
  copy(n, c, 1, column_indeces, 1);
  matrix = matr;
}

TSubMatrix::~TSubMatrix(){
  free(row_indeces);
  free(column_indeces);
  matrix = NULL;
}

double TSubMatrix::Value(const int &i, const int &j){
  return matrix->Value(row_indeces[i], column_indeces[j]);
}

TDifferenceMatrix::TDifferenceMatrix(TMatrix *m, TMatrix *s):
  TMatrix(m->GetRowsNumber(), m->GetColumnsNumber()),
  Minuend_Matrix(NULL),
  Subtrahend_Matrix(NULL){
  if ((m->GetRowsNumber() != s->GetRowsNumber()) || (m->GetColumnsNumber() != s->GetColumnsNumber())){
    this->~TDifferenceMatrix();
    throw(trying_to_subtract_matrices_of_different_sizes);
  }
  else{
    Minuend_Matrix = m;
    Subtrahend_Matrix = s;
  }
}

double TDifferenceMatrix::Value(const int &i, const int &j){
  return Minuend_Matrix->Value(i, j) - Subtrahend_Matrix->Value(i, j);
}
