#include "../include/tt.h"

//Find max element in array a and return scale
int Scale(int size, double *a){
  int scale = 0;
  int idx = iamax(size, a, 1);
  double alpha = a[idx];
  if (fabs(alpha) > 0.0)
    scale = (int) floor(log(fabs(alpha))/log(2));
  return scale;
}

TT::TT():
  TTensor(),
  ref_counter(nullptr),
  orthogonality(-1),
  power_scale(0),
  max_rank(0),
  max_carriages_size(0),
  max_modes_size(0),
  ranks(nullptr),
  carriages(nullptr){
    ref_counter = (unsigned int*) malloc(sizeof(unsigned int));
    *ref_counter = 1u;
}

TT::TT(const int d, int *modes_sizes, int *tt_ranks):
  TTensor(d, modes_sizes),
  ref_counter(nullptr),
  orthogonality(-1),
  power_scale(0),
  max_rank(0),
  max_carriages_size(0),
  max_modes_size(0),
  ranks(nullptr),
  carriages(nullptr){
    ref_counter = (unsigned int*) malloc(sizeof(unsigned int));
    *ref_counter = 1u;
    this->ranks = (int *) malloc((d + 1) * sizeof(int));
    for (int k = 0; k <= d; ++k){
      this->ranks[k] = tt_ranks[k];
      if (this->ranks[k] > this->max_rank)
        this->max_rank = this->ranks[k];
    }
    this->carriages = (double **) malloc(d * sizeof(double*));
    for (int k = 0; k < d; ++k){
      if (this->modes_sizes[k] > this->max_modes_size) this->max_modes_size = this->modes_sizes[k];
      int size = ranks[k] * modes_sizes[k] * ranks[k + 1];
      if (size > this->max_carriages_size) this->max_carriages_size = size;
      this->carriages[k] = (double *) malloc(size * sizeof(double));
      for (int i = 0; i < ranks[k + 1]; ++i)
        for (int j = 0; j < modes_sizes[k]; ++j)
          for (int l = 0; l < ranks[k]; ++l){
            int ind = i * modes_sizes[k] * ranks[k] + j * ranks[k] + l;
            this->carriages[k][ind] = rand() % 1000000000 / 100000000.0;
          }
      int scale = Scale(size, this->carriages[k]);
      this->power_scale += scale;
      scal(size, pow(2, -scale), this->carriages[k], 1);
    }
}

TT::TT(const int d, int *modes_sizes, int *tt_ranks, double **tt_carriages):
  TTensor(d, modes_sizes),
  ref_counter(nullptr),
  orthogonality(-1),
  power_scale(0),
  max_rank(0),
  max_carriages_size(0),
  max_modes_size(0),
  ranks(nullptr),
  carriages(nullptr){
    ref_counter = (unsigned int*) malloc(sizeof(unsigned int));
    *ref_counter = 1u;
    const int kD = d;
    this->ranks = (int *) malloc((kD + 1) * sizeof(int));
    for (int k = 0; k <= d; ++k){
      this->ranks[k] = tt_ranks[k];
      if (this->ranks[k] > this->max_rank)
        this->max_rank = this->ranks[k];
    }
    this->carriages = (double **) malloc(kD * sizeof(double *));
    for (int k = 0; k < kD; ++k){
      if (this->modes_sizes[k] > this->max_modes_size)
        this->max_modes_size = this->modes_sizes[k];
      int size = this->ranks[k] * this->modes_sizes[k] * this->ranks[k + 1];
      if (size > this->max_carriages_size)
        this->max_carriages_size = size;
      this->carriages[k] = (double *) malloc(size * sizeof(double));
      copy(size, tt_carriages[k], 1, this->carriages[k], 1);
      int scale = Scale(size, this->carriages[k]);
      this->power_scale += scale;
      scal(size, pow(2, -scale), this->carriages[k], 1);
  }
}

void TT::Free(){
  if (carriages != nullptr)
    for (int k = 0; k < this->dimensionality; ++k)
      free(carriages[k]);
  free(carriages);
  carriages = nullptr;
  free(ranks);
  ranks = nullptr;
  free(ref_counter);
  ref_counter = nullptr;
}

TT::~TT(){
  --(*ref_counter);
  if (*ref_counter == 0){
    this->Free();
  }
}

TT::TT(const TT &tt):
  TTensor(tt),
  ref_counter(tt.ref_counter),
  orthogonality(tt.orthogonality),
  power_scale(tt.power_scale),
  max_rank(tt.max_rank),
  max_carriages_size(tt.max_carriages_size),
  max_modes_size(tt.max_modes_size),
  ranks(tt.ranks),
  carriages(tt.carriages){
    //std::cout << "copy TT\n";
    ++(*ref_counter);
}

TT::TT(const TT &tt, const double &c):
  TTensor(tt.dimensionality, tt.modes_sizes),
  ref_counter(nullptr),
  orthogonality(-1),
  power_scale(0),
  max_rank(1),
  max_carriages_size(1),
  max_modes_size(0),
  ranks(nullptr),
  carriages(nullptr){
    ref_counter = (unsigned int*) malloc(sizeof(unsigned int));
    *ref_counter = 1u;
    const int kD = this->dimensionality;
    carriages = (double **) malloc(kD * sizeof(double*));
    for (int k = 0; k < kD; ++k)
      if (this->modes_sizes[k] > this->max_modes_size)
        this->max_modes_size = this->modes_sizes[k];
    max_carriages_size = this->max_modes_size;
    this->ranks = (int *) malloc((kD + 1) * sizeof(int));
    this->ranks[kD] = 1;
    for (int k = 0; k < kD; ++k){
      this->ranks[k] = 1;
      int size = this->modes_sizes[k];
      this->carriages[k] = (double *) malloc(size * sizeof(double));
      copy(size, &c, 0, this->carriages[k], 1);
    }
}

/*
TT::TT(TT&& tt) noexcept:
  TTensor(move(tt)),
  ref_counter(move(ref_counter)),
  orthogonality(move(tt.orthogonality)),
  power_scale(move(tt.power_scale)),
  max_rank(move(tt.max_rank)),
  max_carriages_size(move(tt.max_carriages_size)),
  max_modes_size(move(tt.max_modes_size)),
  ranks(move(tt.ranks)),
  carriages(move(tt.carriages)){
    std::cout << "MOVE" << endl;
  this->dimensionality = move(tt.dimensionality);
  this->modes_sizes = move(tt.modes_sizes);
  tt.ref_counter = nullptr;
  tt.ranks = nullptr;
  tt.carriages = nullptr;
  tt.modes_sizes = nullptr;
}
*/
/*
TT & TT::operator=(TT&& tt) noexcept{
  if (this != &tt){
    this->Free();
    TTensor::operator=(move(tt));
    ref_counter = exchange(ref_counter, nullptr);
    orthogonality = exchange(tt.orthogonality, 0);
    power_scale = exchange(tt.power_scale, 0);
    max_rank = exchange(tt.max_rank, 0);
    max_carriages_size = exchange(tt.max_carriages_size, 0);
    max_modes_size = exchange(tt.max_modes_size, 0);
    ranks = exchange(tt.ranksm nullptr);
    carriages = exchange(tt.carriages, nullptr);
  }
  return *this;
}
*/

int CutRank(int rank, const double eps, const int cut_rank, double *a){
  long double s = 0, r = 0;
  //#pragma omp parallel for reduction (+: s)
  for (int i = rank - 1; i >= 0; --i)
    s += ((long double) a[i]) * ((long double) a[i]);
  //#pragma omp barrier
  int new_rank = 0;
  for (int i = rank - 1; i >= 0; --i){
    r += ((long double) a[i]) * ((long double) a[i]);
    if (((long double) s) * ((long double) eps) * ((long double) eps) <= r){
        new_rank = i + 1;
        break;
    }
  }
  if ((cut_rank != 0) && (new_rank > cut_rank))
    new_rank = cut_rank;
  new_rank = std::max(1, new_rank);
  return new_rank;
}

void TT::UpdateMax(){
  const int kD = this->dimensionality;
  this->max_rank = this->ranks[kD];
  this->max_carriages_size = 1;
  for (int k = 0; k < kD; ++k){
    int size = this->ranks[k] * this->modes_sizes[k] * this->ranks[k + 1];
    this->max_carriages_size = std::max(this->max_carriages_size, size);
    this->max_rank = std::max(this->max_rank, this->ranks[k]);
  }
}

double TT::operator[](const int *i) const{
  const int kD = this->dimensionality;
  double *v = (double *) calloc(this->max_rank, sizeof(double));
  double *v1 = (double *) calloc(this->max_rank, sizeof(double));
  int scale = power_scale / kD;
  int mod_scale = power_scale % kD;
  double factor = pow(2.0, scale);
  double double_factor = (mod_scale > 0) ? 2 * factor : factor / 2;
  mod_scale = abs(mod_scale);
  if (mod_scale > 0){
    axpy(this->ranks[kD - 1], double_factor, carriages[kD - 1] + i[kD - 1] * ranks[kD - 1], 1, v, 1);
    --mod_scale;
  }
  else
    axpy(this->ranks[kD - 1], factor, carriages[kD - 1] + i[kD - 1] * ranks[kD - 1], 1, v, 1);
  for (int k = kD - 2; k > 0; --k){
    int lda = this->modes_sizes[k] * ranks[k];
    double tmp_factor = factor;
    if (mod_scale > 0){
      tmp_factor = double_factor;
      --mod_scale;
    }
    gemv(CblasColMajor, CblasNoTrans, ranks[k], ranks[k + 1], tmp_factor, carriages[k] + i[k] * ranks[k], lda, v, 1, 0.0, v1, 1);
    //copy(ranks[k], v1, 1, v, 1);
    std::swap(v1, v);
  }
  double result = factor * dot_u(ranks[1], carriages[0] + i[0] * ranks[0], modes_sizes[0], v, 1);
  free(v1);
  free(v);
  return result;
}

void TT::Copy(){
    if (*ref_counter == 1u)
      return;
    else
      --(*ref_counter);
    ref_counter = (unsigned int*) malloc(sizeof(unsigned int));
    *ref_counter = 1u;
    const int kD = this->dimensionality;
    int *new_modes = (int *) malloc(kD * sizeof(int));
    copy(kD, this->modes_sizes, 1, new_modes, 1);
    this->modes_sizes = new_modes;

    int *new_ranks = (int *) malloc((kD + 1) * sizeof(int));
    copy(kD + 1, this->ranks, 1, new_ranks, 1);
    this->ranks = new_ranks;

    double **new_carriages = (double **) malloc(kD * sizeof(double *));
    for (int k = 0; k < kD; ++k){
      int size = this->ranks[k] * this->modes_sizes[k] * this->ranks[k + 1];
      new_carriages[k] = (double *) malloc(size * sizeof(double));
      copy(size, this->carriages[k], 1, new_carriages[k], 1);
    }
    this->carriages = new_carriages;
}

TT & TT::operator=(const TT &tt){
  if (this != &tt){
    if (*ref_counter == 1u)
      this->Free();
    else
      --(*ref_counter);
    ref_counter = tt.ref_counter;
    ++(*ref_counter);
    if (this->dimensionality != tt.dimensionality)
      this->modes_sizes = (int *) realloc(this->modes_sizes, tt.dimensionality * sizeof(int));
    this->dimensionality = tt.dimensionality;
    copy(this->dimensionality, tt.modes_sizes, 1, this->modes_sizes, 1);
    this->orthogonality = tt.orthogonality;
    this->power_scale = tt.power_scale;
    this->max_rank = tt.max_rank;
    this->max_carriages_size = tt.max_carriages_size;
    this->max_modes_size = tt.max_modes_size;
    this->ranks = tt.ranks;
    this->carriages = tt.carriages;
  }
  return *this;
}

TT & TT::operator+=(const TT &tt){
  this->Copy();
  TT sum;
  const int kD = this->dimensionality;
 	if (kD != tt.dimensionality){
    printf("Operator+ throws 1.\n");
    throw(1);
  }
  sum.dimensionality = kD;
  sum.modes_sizes = (int *) malloc(kD * sizeof(int));
  for (int k = 0; k < kD; ++k){
    if (this->modes_sizes[k] != tt.modes_sizes[k]){
      printf("Operator+ throws 2.\n");
      throw(2);
    }
    sum.modes_sizes[k] = this->modes_sizes[k];
  }
  sum.orthogonality = -1;
  //sum.power_scale and tmp_factor
  int scale = 0;
  int tensor_for_scale = 0;
  if (this->power_scale < tt.power_scale){
    sum.power_scale = tt.power_scale;
    scale = tt.power_scale - this->power_scale;
  }
  else{
    sum.power_scale = this->power_scale;
    scale = this->power_scale - tt.power_scale;
    tensor_for_scale = 1;
  }
  int mod_scale = (scale % kD);
  scale = scale / kD;
  double factor = pow(2.0, -scale);
  double double_factor = factor * 0.5;
  double tmp_factor = (mod_scale == 0) ? factor : double_factor;
  //sum.ranks[0..kD]
  sum.ranks = (int *) malloc((kD + 1) * sizeof(int));
  sum.ranks[0] = 1;
  sum.max_rank = 1;
  for (int k = 1; k < kD; ++k){
    sum.ranks[k] = this->ranks[k] + tt.ranks[k];
    sum.max_rank = std::max(sum.max_rank, sum.ranks[k]);
  }
  sum.ranks[kD] = 1;
  //sum.carriages[0..kD - 1]
  sum.carriages = (double **) malloc(kD * sizeof(double*));
  int size = sum.modes_sizes[0] * sum.ranks[1];
  sum.max_carriages_size = size;
  sum.max_modes_size = this->max_modes_size;
  sum.carriages[0] = (double *) calloc(size, sizeof(double));
  if (tensor_for_scale == 0){
    axpy(sum.modes_sizes[0] * this->ranks[1], tmp_factor, this->carriages[0], 1, sum.carriages[0], 1);
    copy(sum.modes_sizes[0] * tt.ranks[1], tt.carriages[0], 1,
        sum.carriages[0] + sum.modes_sizes[0] * this->ranks[1], 1);
  }
  else{
    copy(sum.modes_sizes[0] * this->ranks[1], this->carriages[0], 1, sum.carriages[0], 1);
    axpy(sum.modes_sizes[0] * tt.ranks[1], tmp_factor, tt.carriages[0], 1,
        sum.carriages[0] + sum.modes_sizes[0] * this->ranks[1], 1);
  }
  //sum.carriages[1..kD - 2]
  for (int k = 1; k < kD - 1; ++k){
    size = sum.ranks[k] * sum.modes_sizes[k] * sum.ranks[k + 1];
    if (size > sum.max_carriages_size)
      sum.max_carriages_size = size;
    sum.carriages[k] = (double *) calloc(size, sizeof(double));
    tmp_factor = (k < mod_scale) ? double_factor : factor;
    for (int i = 0; i < this->ranks[k + 1]; ++i)
      for (int j = 0; j < sum.modes_sizes[k]; ++j){
        double *from = this->carriages[k] + j * this->ranks[k] + i * this->ranks[k] * sum.modes_sizes[k];
        double *to = sum.carriages[k] + j * sum.ranks[k] + i * sum.ranks[k] * sum.modes_sizes[k];
        if (tensor_for_scale == 0)
          axpy(this->ranks[k], tmp_factor, from, 1, to, 1);
        else
          copy(this->ranks[k], from, 1, to, 1);
     }
    for (int i = this->ranks[k + 1]; i < sum.ranks[k + 1]; ++i)
      for (int j = 0; j < sum.modes_sizes[k]; ++j){
        double *from = tt.carriages[k] + j * tt.ranks[k] + (i - this->ranks[k + 1]) * tt.ranks[k] * sum.modes_sizes[k];
        double *to = sum.carriages[k] + j * sum.ranks[k] + i * sum.ranks[k] * sum.modes_sizes[k] + this->ranks[k];
        if (tensor_for_scale == 0)
          copy(tt.ranks[k], from, 1, to, 1);
        else
          axpy(tt.ranks[k], tmp_factor, from, 1, to, 1);
      }
  }
  //sum.carriages[kD - 1]
  size = sum.ranks[kD - 1] * sum.modes_sizes[kD - 1] * sum.ranks[kD];
  if (size > sum.max_carriages_size) sum.max_carriages_size = size;
  sum.carriages[kD - 1] = (double *) calloc(size, sizeof(double));
  for (int j = 0; j < sum.modes_sizes[kD - 1]; ++j)
    if (tensor_for_scale == 0){
      axpy(this->ranks[kD - 1], factor, this->carriages[kD - 1] + j * this->ranks[kD - 1], 1,
  	      sum.carriages[kD - 1] + j * sum.ranks[kD - 1], 1);
      copy(tt.ranks[kD - 1], tt.carriages[kD - 1] + j * tt.ranks[kD - 1], 1,
  	      sum.carriages[kD - 1] + j * sum.ranks[kD - 1] + this->ranks[kD - 1], 1);
    }
    else{
      copy(this->ranks[kD - 1], this->carriages[kD - 1] + j * this->ranks[kD - 1], 1,
          sum.carriages[kD - 1] + j * sum.ranks[kD - 1], 1);
      axpy(tt.ranks[kD - 1], factor, tt.carriages[kD - 1] + j * tt.ranks[kD - 1], 1,
          sum.carriages[kD - 1] + j * sum.ranks[kD - 1] + this->ranks[kD - 1], 1);
    }
  *this = sum;
  return *this;
}

TT & TT::operator-=(const TT &tt){
  *this += tt * (-1.0);
  return *this;
}

TT & TT::operator*=(const TT &tt){
  this->Copy();
  TT product;
  const int kD = this->dimensionality;
 	if (kD != tt.dimensionality){
    printf("Operator* throws 1.\n");
    throw(1);
  }
  product.dimensionality = kD;
  product.modes_sizes = (int *) malloc(kD * sizeof(int));
  for (int k = 0; k < kD; ++k){
    if (this->modes_sizes[k] != tt.modes_sizes[k]){
      printf("Operator* throws 2.\n");
      throw(2);
    }
    product.modes_sizes[k] = this->modes_sizes[k];
  }
  product.orthogonality = -1;
  product.power_scale = this->power_scale + tt.power_scale;
  // product.ranks[0..kD]
  product.ranks = (int *) malloc((kD + 1) * sizeof(int));
  product.ranks[0] = 1;
  product.max_rank = 1;
  for (int k = 1; k < kD; ++k){
    product.ranks[k] = this->ranks[k] * tt.ranks[k];
    product.max_rank = std::max(product.max_rank, product.ranks[k]);
  }
  product.ranks[kD] = 1;
  product.max_carriages_size = 1;
  product.max_modes_size = this->max_modes_size;
  //product.carriages[0..kD - 1]
  product.carriages = (double **) malloc(kD * sizeof(double*));
  for (int k = 0; k < kD; ++k){
    int size = product.ranks[k] * product.modes_sizes[k] * product.ranks[k + 1];
    product.carriages[k] = (double *) calloc(size, sizeof(double));
    if (product.carriages[k] == nullptr){
      printf("Operator* throws 3.\n");
      throw(3);
    }
    if (size > product.max_carriages_size) product.max_carriages_size = size;
    // Kroneker product of this->carriages[k] and tt.carriages[k]
    for (int i = 0; i < product.modes_sizes[k]; ++i)
      for (int cols = 0; cols < this->ranks[k + 1]; ++cols)
        for (int rows = 0; rows < this->ranks[k]; ++rows){
          int ind = cols * this->ranks[k] * this->modes_sizes[k] + i * this->ranks[k] + rows;
          for (int j = 0; j < tt.ranks[k + 1]; ++j){
            double *x = tt.carriages[k] + j * tt.ranks[k] * tt.modes_sizes[k] + i * tt.ranks[k];
            int column = (cols * tt.ranks[k + 1] + j) * product.ranks[k] * product.modes_sizes[k];
            double *y = product.carriages[k] + column + i * product.ranks[k] + rows * tt.ranks[k];
            axpy(tt.ranks[k], this->carriages[k][ind], x, 1, y, 1);
          }
        }
  }
  *this = product;
  return *this;
}

TT & TT::Apply_mu(const TT &tt, int mu){
  this->Copy();
  TT product;
  const int kD = this->dimensionality;
 	if (kD != tt.dimensionality){
    printf("Operator* throws 1.\n");
    throw(1);
  }
  product.dimensionality = kD;
  product.modes_sizes = (int *) malloc(kD * sizeof(int));
  for (int k = 0; k < kD; ++k){
    if (this->modes_sizes[k] != tt.modes_sizes[k]){
      printf("Operator* throws 2.\n");
      throw(2);
    }
    product.modes_sizes[k] = this->modes_sizes[k];
  }
  product.orthogonality = -1;
  product.power_scale = this->power_scale + tt.power_scale;
  // product.ranks[0..kD]
  product.ranks = (int *) malloc((kD + 1) * sizeof(int));
  product.ranks[0] = 1;
  product.max_rank = 1;
  for (int k = 1; k < kD; ++k){
    product.ranks[k] = this->ranks[k] * tt.ranks[k];
    product.max_rank = std::max(product.max_rank, product.ranks[k]);
  }
  product.ranks[kD] = 1;
  product.max_carriages_size = 1;
  product.max_modes_size = this->max_modes_size;
  //product.carriages[0..kD - 1]
  product.carriages = (double **) malloc(kD * sizeof(double*));
  for (int k = 0; k < kD; ++k){
    int size = product.ranks[k] * product.modes_sizes[k] * product.ranks[k + 1];
    //if (k == mu) size = product.modes_sizes[k];
    product.carriages[k] = (double *) calloc(size, sizeof(double));
    if (product.carriages[k] == nullptr){
      printf("Operator* throws 3.\n");
      throw(3);
    }
    if (size > product.max_carriages_size) product.max_carriages_size = size;
    if (k == mu) continue;
    // Kroneker product of this->carriages[k] and tt.carriages[k]
    for (int i = 0; i < product.modes_sizes[k]; ++i)
      for (int cols = 0; cols < this->ranks[k + 1]; ++cols)
        for (int rows = 0; rows < this->ranks[k]; ++rows){
          int ind = cols * this->ranks[k] * this->modes_sizes[k] + i * this->ranks[k] + rows;
          for (int j = 0; j < tt.ranks[k + 1]; ++j){
            double *x = tt.carriages[k] + j * tt.ranks[k] * tt.modes_sizes[k] + i * tt.ranks[k];
            int column = (cols * tt.ranks[k + 1] + j) * product.ranks[k] * product.modes_sizes[k];
            double *y = product.carriages[k] + column + i * product.ranks[k] + rows * tt.ranks[k];
            axpy(tt.ranks[k], this->carriages[k][ind], x, 1, y, 1);
          }
        }
  }
  *this = product;
  return *this;
}

TT & TT::operator*=(double alpha){
  this->Copy();
  const int kD = this->dimensionality;
  if (fabs(alpha) > 0.0){
    int scale = (int) floor(log(fabs(alpha))/log(2));
    alpha /= pow(2, scale);
    this->power_scale += scale;
  }
  else{
    this->power_scale = 0;
    this->orthogonality = -1;
    this->max_rank = 1;
    for (int k = 0; k <= kD; ++k)
      this->ranks[k] = 1;
    for (int k = 0; k < kD; ++k){
      this->max_modes_size = std::max(this->max_modes_size, this->modes_sizes[k]);
      this->carriages[k] = (double *) realloc(this->carriages[k], this->modes_sizes[k] * sizeof(double));
      for (int i = 0; i < this->modes_sizes[k]; ++i)
        this->carriages[k][i] = 0;
    }
    this->max_carriages_size = this->max_modes_size;
  }
  if (this->orthogonality == -1){
    int size = this->ranks[0] * this->modes_sizes[0] * this->ranks[1];
    scal(size, alpha, this->carriages[0], 1);
  }
  else{
    int mu = this->orthogonality;
    int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1];
    scal(size, alpha, this->carriages[mu], 1);
  }
  return *this;
}

/*TT operator+(TT lhs, const TT &rhs){
  return lhs += rhs;
}*/

TT TT::operator+(const TT &tt) const{
  TT sum;
  const int kD = this->dimensionality;
 	if (kD != tt.dimensionality){
    printf("Operator+ throws 1.\n");
    throw(1);
  }
  sum.dimensionality = kD;
  sum.modes_sizes = (int *) malloc(kD * sizeof(int));
  for (int k = 0; k < kD; ++k){
    if (this->modes_sizes[k] != tt.modes_sizes[k]){
      printf("Operator+ throws 2.\n");
      throw(2);
    }
    sum.modes_sizes[k] = this->modes_sizes[k];
  }
  sum.orthogonality = -1;
  //sum.power_scale and tmp_factor
  int scale = 0;
  int tensor_for_scale = 0;
  if (this->power_scale < tt.power_scale){
    sum.power_scale = tt.power_scale;
    scale = tt.power_scale - this->power_scale;
  }
  else{
    sum.power_scale = this->power_scale;
    scale = this->power_scale - tt.power_scale;
    tensor_for_scale = 1;
  }
  int mod_scale = (scale % kD);
  scale = scale / kD;
  double factor = pow(2.0, -scale);
  double double_factor = factor * 0.5;
  double tmp_factor = (mod_scale == 0) ? factor : double_factor;
  //sum.ranks[0..kD]
  sum.ranks = (int *) malloc((kD + 1) * sizeof(int));
  sum.ranks[0] = 1;
  sum.max_rank = 1;
  for (int k = 1; k < kD; ++k){
    sum.ranks[k] = this->ranks[k] + tt.ranks[k];
    sum.max_rank = std::max(sum.max_rank, sum.ranks[k]);
  }
  sum.ranks[kD] = 1;
  //sum.carriages[0..kD - 1]
  sum.carriages = (double **) malloc(kD * sizeof(double*));
  int size = sum.modes_sizes[0] * sum.ranks[1];
  sum.max_carriages_size = size;
  sum.max_modes_size = this->max_modes_size;
  sum.carriages[0] = (double *) calloc(size, sizeof(double));
  if (tensor_for_scale == 0){
    axpy(sum.modes_sizes[0] * this->ranks[1], tmp_factor, this->carriages[0], 1, sum.carriages[0], 1);
    copy(sum.modes_sizes[0] * tt.ranks[1], tt.carriages[0], 1,
        sum.carriages[0] + sum.modes_sizes[0] * this->ranks[1], 1);
  }
  else{
    copy(sum.modes_sizes[0] * this->ranks[1], this->carriages[0], 1, sum.carriages[0], 1);
    axpy(sum.modes_sizes[0] * tt.ranks[1], tmp_factor, tt.carriages[0], 1,
        sum.carriages[0] + sum.modes_sizes[0] * this->ranks[1], 1);
  }
  //sum.carriages[1..kD - 2]
  for (int k = 1; k < kD - 1; ++k){
    size = sum.ranks[k] * sum.modes_sizes[k] * sum.ranks[k + 1];
    if (size > sum.max_carriages_size)
      sum.max_carriages_size = size;
    sum.carriages[k] = (double *) calloc(size, sizeof(double));
    tmp_factor = (k < mod_scale) ? double_factor : factor;
    for (int i = 0; i < this->ranks[k + 1]; ++i)
      for (int j = 0; j < sum.modes_sizes[k]; ++j){
        double *from = this->carriages[k] + j * this->ranks[k] + i * this->ranks[k] * sum.modes_sizes[k];
        double *to = sum.carriages[k] + j * sum.ranks[k] + i * sum.ranks[k] * sum.modes_sizes[k];
        if (tensor_for_scale == 0)
          axpy(this->ranks[k], tmp_factor, from, 1, to, 1);
        else
          copy(this->ranks[k], from, 1, to, 1);
     }
    for (int i = this->ranks[k + 1]; i < sum.ranks[k + 1]; ++i)
      for (int j = 0; j < sum.modes_sizes[k]; ++j){
        double *from = tt.carriages[k] + j * tt.ranks[k] + (i - this->ranks[k + 1]) * tt.ranks[k] * sum.modes_sizes[k];
        double *to = sum.carriages[k] + j * sum.ranks[k] + i * sum.ranks[k] * sum.modes_sizes[k] + this->ranks[k];
        if (tensor_for_scale == 0)
          copy(tt.ranks[k], from, 1, to, 1);
        else
          axpy(tt.ranks[k], tmp_factor, from, 1, to, 1);
      }
  }
  //sum.carriages[kD - 1]
  size = sum.ranks[kD - 1] * sum.modes_sizes[kD - 1] * sum.ranks[kD];
  if (size > sum.max_carriages_size) sum.max_carriages_size = size;
  sum.carriages[kD - 1] = (double *) calloc(size, sizeof(double));
  for (int j = 0; j < sum.modes_sizes[kD - 1]; ++j)
    if (tensor_for_scale == 0){
      axpy(this->ranks[kD - 1], factor, this->carriages[kD - 1] + j * this->ranks[kD - 1], 1,
  	      sum.carriages[kD - 1] + j * sum.ranks[kD - 1], 1);
      copy(tt.ranks[kD - 1], tt.carriages[kD - 1] + j * tt.ranks[kD - 1], 1,
  	      sum.carriages[kD - 1] + j * sum.ranks[kD - 1] + this->ranks[kD - 1], 1);
    }
    else{
      copy(this->ranks[kD - 1], this->carriages[kD - 1] + j * this->ranks[kD - 1], 1,
          sum.carriages[kD - 1] + j * sum.ranks[kD - 1], 1);
      axpy(tt.ranks[kD - 1], factor, tt.carriages[kD - 1] + j * tt.ranks[kD - 1], 1,
          sum.carriages[kD - 1] + j * sum.ranks[kD - 1] + this->ranks[kD - 1], 1);
    }
  return sum;
}

TT operator-(TT lhs, const TT &rhs){
  return lhs -= rhs;
}
TT operator*(TT lhs, const TT &rhs){
  return lhs *= rhs;
}

TT TT::operator*(double alpha) const{
  TT new_tt = *this;
  new_tt *= alpha;
  return new_tt;
}

int TT::GetRank(int &i) const{
  return ranks[i];
}

int TT::GetMaxRank() const{
  return this->max_rank;
}

int TT::GetPowerScale() const{
  return this->power_scale;
}

double TT::Nrm2(){
  const int kD = this->dimensionality;
  if (this->orthogonality == kD - 1)
    return nrm2(this->ranks[kD - 1] * this->modes_sizes[kD - 1] * this->ranks[kD], this->carriages[kD - 1], 1);
  else if (this->orthogonality == 0)
    return nrm2(this->ranks[0] * this->modes_sizes[0] * this->ranks[1], this->carriages[0], 1);
  else{
    this->LeftToRightOrthogonalization();
    return nrm2(this->ranks[kD - 1] * this->modes_sizes[kD - 1] * this->ranks[kD], this->carriages[kD - 1], 1);
  }
}

double TT::LogNorm(){
  double nrm2 = this->Nrm2();
  if (nrm2 > 0.0)
    return this->power_scale + log(nrm2)/log(2);
  else
    return 0.0;
}

void TT::Normalize(){
  double nrm2 = this->Nrm2();
  this->power_scale = 0;
  if (nrm2 > 0)
    *this *= 1.0/nrm2;
  else
    *this *= 0.0;
}

double TT::Dot(const TT &a) const{
  const int kD = this->dimensionality;
  double *p = (double *) malloc(a.max_rank * this->max_rank * sizeof(double));
  double *v1 = (double *) malloc(a.max_rank * this->max_modes_size * this->max_rank * sizeof(double));
  int scale = (this->power_scale + a.power_scale) / kD;
  int mod_scale = (this->power_scale + a.power_scale) % kD;
  double factor = pow(2.0, scale);
  double double_factor = (mod_scale > 0) ? 2 * factor : factor / 2;
  mod_scale = abs(mod_scale);
  double tmp_factor = (mod_scale == 0) ? factor : double_factor;
  int lda = this->modes_sizes[0] * this->ranks[0];
  // p = (this->carriages[0]^L)^T * a.carriages[0]^L
  gemm(CblasColMajor, CblasTrans, CblasNoTrans, this->ranks[1], a.ranks[1], lda, tmp_factor,
      this->carriages[0], lda, a.carriages[0], this->modes_sizes[0] * a.ranks[0], 0.0, p, this->ranks[1]);
  for (int k = 1; k < kD; ++k){
    tmp_factor = (k < mod_scale) ? double_factor : factor;
    // v1 = p * (a.carriages[k])^R
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, this->ranks[k], a.modes_sizes[k] * a.ranks[k + 1], a.ranks[k],
        1.0, p, this->ranks[k], a.carriages[k], a.ranks[k], 0.0, v1, this->ranks[k]);
    lda = this->modes_sizes[k] * this->ranks[k];
    // p = (this->carriages[k])^T * v1^L
    gemm(CblasColMajor, CblasTrans, CblasNoTrans, this->ranks[k + 1], a.ranks[k + 1], lda, tmp_factor,
        this->carriages[k], lda, v1, lda, 0.0, p, this->ranks[k + 1]);
  }
  double result = p[0];
  free(p);
  free(v1);
  return result;
}

double * TT::LeftToRightDot(const TT &a, const int mu) const{
  if (mu == 0) return nullptr;
  const int kD = this->dimensionality;
  double *p = (double *) malloc(a.max_rank * this->max_rank * sizeof(double));
  double *v1 = (double *) malloc(a.max_rank * this->max_modes_size * this->max_rank * sizeof(double));
  int scale = (this->power_scale + a.power_scale) / kD;
  int mod_scale = (this->power_scale + a.power_scale) % kD;
  double factor = pow(2.0, scale);
  double double_factor = (mod_scale > 0) ? 2 * factor : factor / 2;
  mod_scale = abs(mod_scale);
  double tmp_factor = (mod_scale == 0) ? factor : double_factor;
  int lda = this->modes_sizes[0] * this->ranks[0];
  // p = (this->carriages[0])^L^T * a.carriages[0]^L
  gemm(CblasColMajor, CblasTrans, CblasNoTrans, this->ranks[1], a.ranks[1], lda, tmp_factor,
      this->carriages[0], lda, a.carriages[0], this->modes_sizes[0] * a.ranks[0], 0.0, p, this->ranks[1]);
  for (int k = 1; k < mu; ++k){
    tmp_factor = (k < mod_scale) ? double_factor : factor;
    // v1 = p * (a.carriages[k])^R
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, this->ranks[k], a.modes_sizes[k] * a.ranks[k + 1], a.ranks[k],
        1.0, p, this->ranks[k], a.carriages[k], a.ranks[k], 0.0, v1, this->ranks[k]);
    lda = this->modes_sizes[k] * this->ranks[k];
    // p = (this->carriages[k])^T * v1^L
    gemm(CblasColMajor, CblasTrans, CblasNoTrans, this->ranks[k + 1], a.ranks[k + 1], lda, tmp_factor,
        this->carriages[k], lda, v1, lda, 0.0, p, this->ranks[k + 1]);
  }
  double *result = (double *) malloc(this->ranks[mu] * a.ranks[mu] * sizeof(double));
  copy(this->ranks[mu] * a.ranks[mu], p, 1, result, 1);
  free(p);
  free(v1);
  return result;
}

double * TT::RightToLeftDot(const TT &a, const int mu) const{
  if (mu == this->dimensionality - 1) return nullptr;
  const int kD = this->dimensionality;
  double *p = (double *) malloc(a.max_rank * this->max_rank * sizeof(double));
  double *v1 = (double *) malloc(a.max_rank * this->max_modes_size * this->max_rank * sizeof(double));
  int scale = (this->power_scale + a.power_scale) / kD;
  int mod_scale = (this->power_scale + a.power_scale) % kD;
  double factor = pow(2.0, scale);
  double double_factor = (mod_scale > 0) ? 2 * factor : factor / 2;
  mod_scale = abs(mod_scale);
  double tmp_factor = factor;
  int lda = this->modes_sizes[kD - 1] * this->ranks[kD];
  // p = (this->carriages[kD-1])^R * a.carriages[kD-1]^R^T
  gemm(CblasColMajor, CblasNoTrans, CblasTrans, this->ranks[kD - 1], a.ranks[kD - 1], lda, tmp_factor,
      this->carriages[kD - 1], this->ranks[kD - 1], a.carriages[kD - 1], a.ranks[kD - 1], 0.0, p, this->ranks[kD - 1]);
  for (int k = kD - 2; k > mu; --k){
    tmp_factor = (k < mod_scale) ? double_factor : factor;
    // v1 = (this->carriages[k])^L * p
    lda = this->modes_sizes[k] * this->ranks[k];
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, lda, a.ranks[k + 1], this->ranks[k + 1],
        1.0, this->carriages[k], lda, p, this->ranks[k + 1], 0.0, v1, lda);
    // p =  v1^R * (a.carriages[k])^R^T
    lda = a.modes_sizes[k] * a.ranks[k + 1];
    gemm(CblasColMajor, CblasNoTrans, CblasTrans, this->ranks[k], a.ranks[k], lda, tmp_factor,
        v1, this->ranks[k], a.carriages[k], a.ranks[k], 0.0, p, this->ranks[k]);
  }
  double *result = (double *) malloc(this->ranks[mu + 1] * a.ranks[mu + 1] * sizeof(double));
  copy(this->ranks[mu + 1] * a.ranks[mu + 1], p, 1, result, 1);
  free(p);
  free(v1);
  return result;
}

TT TT::ConvertFromTTensorTrain(const TTensorTrain &tensor_train){
  TT new_tt;
  new_tt.dimensionality = tensor_train.GetDimensionality();
  new_tt.orthogonality = -1;
  new_tt.max_rank = 1;
  new_tt.power_scale = 0;
  const int kD = new_tt.dimensionality;
  new_tt.carriages = (double **) malloc(kD * sizeof(double*));
  new_tt.modes_sizes = (int *) malloc(kD * sizeof(int));
  copy(kD, tensor_train.modes_sizes, 1, new_tt.modes_sizes, 1);
  new_tt.ranks = (int *) malloc((kD + 1) * sizeof(int));
  copy(kD + 1, tensor_train.ranks, 1, new_tt.ranks, 1);
  for (int k = 0; k < kD; ++k){
    new_tt.max_modes_size = std::max(new_tt.max_modes_size, new_tt.modes_sizes[k]);
    new_tt.max_rank = std::max(new_tt.max_rank, new_tt.ranks[k]);
    int size = new_tt.ranks[k] * new_tt.modes_sizes[k] * new_tt.ranks[k + 1];
    new_tt.max_carriages_size = std::max(new_tt.max_carriages_size, size);
    new_tt.carriages[k] = (double *) malloc(size * sizeof(double));
    //copy(size, new_tt.carriages[k], 1, tensor_train.carriages[k], 1);
    for (int i = 0; i < new_tt.ranks[k]; ++i)
      for (int j = 0; j < new_tt.modes_sizes[k]; ++j)
        for (int l = 0; l < new_tt.ranks[k + 1]; ++l){
          int q = l * new_tt.ranks[k] * new_tt.modes_sizes[k] + j * new_tt.ranks[k] + i;
          int w = j * new_tt.ranks[k] * new_tt.ranks[k + 1] + i * new_tt.ranks[k + 1] + l;
          new_tt.carriages[k][q] = tensor_train.carriages[k][w];
        }
  }
  new_tt.LeftToRightOrthogonalization();
  return new_tt;
}

TTensorTrain TT::ConvertToTTensorTrain(){
  // TT friend of TTensorTrain
  TTensorTrain new_train;
  new_train.dimensionality = this->GetDimensionality();
  const int kD = new_train.dimensionality;
  new_train.carriages = (double **) malloc(kD * sizeof(double*));
  new_train.modes_sizes = (int *) malloc(kD * sizeof(int));
  copy(kD, this->modes_sizes, 1, new_train.modes_sizes, 1);
  new_train.ranks = (int *) malloc((kD + 1) * sizeof(int));
  copy(kD + 1, this->ranks, 1, new_train.ranks, 1);
  for (int k = 0; k < kD; ++k){
    int size = this->ranks[k] * this->modes_sizes[k] * this->ranks[k + 1];
    new_train.carriages[k] = (double *) malloc(size * sizeof(double));
    //copy(size, new_train.carriages[k], 1, this->carriages[k], 1);
    for (int i = 0; i < this->ranks[k]; ++i)
      for (int j = 0; j < this->modes_sizes[k]; ++j)
        for (int l = 0; l < this->ranks[k + 1]; ++l){
          int q = l * this->ranks[k] * this->modes_sizes[k] + j * this->ranks[k] + i;
          int w = j * this->ranks[k] * this->ranks[k + 1] + i * this->ranks[k + 1] + l;
          new_train.carriages[k][w] = this->carriages[k][q];
        }
  }
  return new_train;
}

void TT::Approximate(TTensor *tt, const TTensorTrainParameters &parameters){
  this->Copy();
  TTensorTrain new_tt;
  new_tt.Approximate(tt, parameters);
  *this = this->ConvertFromTTensorTrain(new_tt);
  return;
}

void TT::LeftToRightOrthogonalization(const int mu){
  if (mu == 0) return;
  this->Copy();
  const int kD = this->dimensionality;
  if (mu == kD - 1) this->orthogonality = kD - 1;
  double *buffer = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *R = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *tau = (double *) malloc(this->max_rank * sizeof(double));
  double zero = 0.0;
  for (int k = 0; k < mu; ++k){
    // QR of this->carriages[k] = ranks[k] * modes_sizes[k] x ranks[k + 1]
    int m = this->ranks[k] * this->modes_sizes[k]; // number of rows
    int n = this->ranks[k + 1]; // number of columns
    int rank = std::min(m, n);
    LAPACKE_dgeqrf(CblasColMajor, m, n, this->carriages[k], m, tau);
    // Copy matrix R = rank x n from this->carriages[k]
    for (int i = 0; i < n; ++i){
      copy(std::min((i + 1), m), this->carriages[k] + i * m, 1, R + i * rank, 1);
      if (i < m) copy(m - (i + 1), &zero, 0, R + i * rank + i + 1, 1);
    }
    LAPACKE_dorgqr(CblasColMajor, m, rank, rank, this->carriages[k], m, tau);  // Compute matrix Q
    // Update ranks and this->carriages[k]
    this->ranks[k + 1] = rank;
    int size = this->ranks[k] * this->modes_sizes[k] * rank;
    this->carriages[k] = (double *) realloc(this->carriages[k], size * sizeof(double));
    // Find max element in R and scale
    int scale = Scale(rank * n, R);
    this->power_scale += scale;
    // R * this->carriages[k + 1] // k + 1 == mu?
    size = rank * this->modes_sizes[k + 1] * this->ranks[k + 2];  // new size of this->carriages[k + 1]
    if (m >= n) // R - Upper-triangular
      trmm(CblasColMajor, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, n, modes_sizes[k + 1] * ranks[k + 2],
          pow(2, -scale), R, rank, this->carriages[k + 1], n);
    else{  // R - Upper-trapezoidal
      gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, rank, modes_sizes[k + 1] * ranks[k + 2], n, pow(2, -scale),
          R, rank, this->carriages[k + 1], n, 0.0, buffer, rank);  // last gemm is gemv
      this->carriages[k + 1] = (double *) realloc(this->carriages[k + 1], size * sizeof(double));
      copy(size, buffer, 1, this->carriages[k + 1], 1);
    }
  }
  // Scale this->carriages[mu]
  int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1];
  int scale = Scale(size, this->carriages[mu]);
  this->power_scale += scale;
  scal(size, pow(2, -scale), this->carriages[mu], 1);
  // Update max_rank and max_carriages_size
  this->UpdateMax();
  free(tau);
  free(R);
  free(buffer);
  return;
}

void TT::RightToLeftOrthogonalization(const int mu){
  if (mu == this->dimensionality - 1) return;
  this->Copy();
  const int kD = this->dimensionality;
  if (mu == 0) this->orthogonality = 0;
  double *buffer = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *L = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *tau = (double *) malloc(this->max_rank * sizeof(double));
  double zero = 0.0;
  for (int k = kD - 1; k > mu; --k){
    // LQ of this->carriages[k] = ranks[k] x modes_sizes[k] * ranks[k + 1]
    int m = this->ranks[k];  // number of rows
    int n = this->modes_sizes[k] * this->ranks[k + 1];  // number of columns
    int rank = std::min(m, n);
    LAPACKE_dgelqf(CblasColMajor, m, n, this->carriages[k], m, tau);
    // Copy matrix L = m x rank from this->carriages[k]
    for (int i = 0; i < rank; ++i){
      copy(m - i, this->carriages[k] + i * m + i, 1, L + i * m + i, 1);
      copy(i, &zero, 0, L + i * m, 1);
    }
    LAPACKE_dorglq(CblasColMajor, rank, n, rank, this->carriages[k], m, tau);  // Compute matrix Q
    // Update ranks and this->carriages[k]
    this->ranks[k] = rank;
    int size = rank * this->modes_sizes[k] * this->ranks[k + 1];
    this->carriages[k] = (double *) realloc(this->carriages[k], size * sizeof(double));
    // Find max element in R and scale
    int scale = Scale(m * rank, L);
    this->power_scale += scale;
    // this->carriages[k - 1] * L // k - 1 == mu?
    size = this->ranks[k - 1] * this->modes_sizes[k - 1] * rank;  // new size of this->carriages[k - 1]
    if (m <= n){ // L - Lower-triangular
      int ldb = this->ranks[k - 1] * this->modes_sizes[k - 1];
      trmm(CblasColMajor, CblasRight, CblasLower, CblasNoTrans, CblasNonUnit, ldb, m,
          pow(2, -scale), L, m, this->carriages[k - 1], ldb);
    }
    else{  // L - Lower-trapezoidal
      int lda = this->ranks[k - 1] * this->modes_sizes[k - 1];
      gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, lda, rank, m,
        pow(2, -scale), this->carriages[k - 1], lda, L, m, 0.0, buffer, lda);
      this->carriages[k - 1] = (double *) realloc(this->carriages[k - 1], size * sizeof(double));
      copy(size, buffer, 1, this->carriages[k - 1], 1);
    }
  }
  // Scale this->carriages[mu]
  int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1];
  int scale = Scale(size, this->carriages[mu]);
  this->power_scale += scale;
  scal(size, pow(2, -scale), this->carriages[mu], 1);
  // Update max_rank and max_carriages_size
  this->UpdateMax();
  free(tau);
  free(L);
  free(buffer);
  return;
}

void TT::LeftToRightOrthogonalization(){
  this->LeftToRightOrthogonalization(this->dimensionality - 1);
}

void TT::LeftToRightCompression(const double eps, const int cut_rank, const int mu){
  this->Copy();
  const int kD = this->dimensionality;
  if (this->orthogonality != 0) // need to change
    this->RightToLeftOrthogonalization();
  double *buffer = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *U = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *tmp = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *VT = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *sigma = (double *) malloc(this->max_rank * sizeof(double));
  double *superb = (double *) malloc((this->max_rank - 1) * sizeof(double));
  copy(this->ranks[0] * this->modes_sizes[0] * this->ranks[1], this->carriages[0], 1, buffer, 1);
  for (int k = 0; k < mu; ++k){
    // this->carriages[k] = ranks[k] * modes_sizes[k] x ranks[k + 1]
    int m = this->ranks[k] * this->modes_sizes[k];  // number of rows
    int n = this->ranks[k + 1]; // number of columns
    int rank = std::min(m, n);
    gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, buffer, m, sigma, U, m, tmp, rank, superb);
    int new_rank = CutRank(rank, eps, cut_rank, sigma);
    int scale = Scale(new_rank, sigma);
    this->power_scale += scale;
    double factor = pow(2, -scale);
    for (int i = 0; i < new_rank; ++i){
      scal(n, factor * sigma[i], tmp + i, rank);
      copy(n, tmp + i, rank, VT + i, new_rank);
    }
    //this->carriages[k] = U;
    this->carriages[k] = (double *) realloc(this->carriages[k], m * new_rank * sizeof(double));
    copy(m * new_rank, U, 1, this->carriages[k], 1);
    this->ranks[k + 1] = new_rank;
    // VT * this->carriages[k + 1], this->carriages[k + 1] = ranks[k + 1] x modes_sizes[k + 1] * ranks[k + 2]
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, new_rank, this->modes_sizes[k + 1] * this->ranks[k + 2], n, 1.0,
			  VT, new_rank, this->carriages[k + 1], n, 0.0, buffer, new_rank);
	}
  int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu + 1];
  this->carriages[mu] = (double *) realloc(this->carriages[mu], size * sizeof(double));
  copy(size, buffer, 1, this->carriages[mu], 1);
  int scale = Scale(size, this->carriages[mu]);
  this->power_scale += scale;
  scal(size, pow(2, -scale), this->carriages[mu], 1);
  // Update max_rank and max_carriages_size
  this->UpdateMax();
  if (mu == kD - 1) // need to change
    this->orthogonality = mu;
  free(superb);
  free(sigma);
  free(VT);
  free(tmp);
  free(U);
  free(buffer);
}

void TT::LeftToRightCompression(const double eps, const int cut_rank){
  this->LeftToRightCompression(eps, cut_rank, this->dimensionality - 1);
}

void TT::RightToLeftCompression(const double eps, const int cut_rank, const int mu){
  this->Copy();
  const int kD = this->dimensionality;
  if (this->orthogonality != kD - 1) // need to change
    this->LeftToRightOrthogonalization();
  double *buffer = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *U = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *VT = (double *) malloc(this->max_carriages_size * sizeof(double));
  double *sigma = (double *) malloc(this->max_rank * sizeof(double));
  double *superb = (double *) malloc((this->max_rank - 1) * sizeof(double));
  copy(this->ranks[kD - 1] * this->modes_sizes[kD - 1] * this->ranks[kD], this->carriages[kD - 1], 1, buffer, 1);
  for (int k = kD - 1; k > mu; --k){
    // this->carriages[k] = ranks[k] x modes_sizes[k] * ranks[k + 1]
    int m = this->ranks[k];  // number of rows
    int n = this->modes_sizes[k] * this->ranks[k + 1]; // number of columns
    int rank = std::min(m, n);
    gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, buffer, m, sigma, U, m, VT, rank, superb);
    int new_rank = CutRank(rank, eps, cut_rank, sigma);
    this->carriages[k] = (double *) realloc(this->carriages[k], new_rank * n * sizeof(double));
    int scale = Scale(new_rank, sigma);
    this->power_scale += scale;
    double factor = pow(2, -scale);
    for (int i = 0; i < new_rank; ++i){
      scal(m, factor * sigma[i], U + i * m, 1);
      copy(n, VT + i, rank, this->carriages[k] + i, new_rank); // this->carriages[k] = VT
    }
    // this->carriages[k - 1] * U, this->carriages[k - 1] = ranks[k - 1] * modes_sizes[k - 1] x ranks[k]
    int lda = this->ranks[k - 1] * this->modes_sizes[k - 1];
    gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, lda, new_rank, m, 1.0,
        this->carriages[k - 1], lda, U, m, 0.0, buffer, lda);
    this->ranks[k] = new_rank;
	}
  int size = this->ranks[mu] * this->modes_sizes[mu] * this->ranks[mu+1];
  this->carriages[mu] = (double *) realloc(this->carriages[mu], size * sizeof(double));
  copy(size, buffer, 1, this->carriages[mu], 1);
  int scale = Scale(size, this->carriages[mu]);
  this->power_scale += scale;
  scal(size, pow(2, -scale), this->carriages[mu], 1);
  // Update max_rank and max_carriages_size
  this->UpdateMax();
  if (mu == 0) // need to change
    this->orthogonality = 0;
  free(buffer);
  free(U);
  free(VT);
  free(sigma);
  free(superb);
}

void TT::SVDCompression(const double eps, const int cut_rank, const int mu){
  this->Copy();
  if (this->orthogonality == this->dimensionality - 1)
    this->RightToLeftCompression(eps, cut_rank);
  else if (this->orthogonality == 0)
    this->LeftToRightCompression(eps, cut_rank);
  else{
    this->LeftToRightOrthogonalization();
    this->RightToLeftCompression(eps, cut_rank);
    //this->RightToLeftOrthogonalization();
    //this->LeftToRightCompression(eps, cut_rank);
  }
}

void TT::Orthogonalize(const int mu){
  if (this->orthogonality != mu){
    this->LeftToRightOrthogonalization(mu);
    this->RightToLeftOrthogonalization(mu);
    this->orthogonality = mu;
  }
}

double * TT::MakeLocalSystem(TT &x, int mu, const double eps, int cut_rank) const{
  x.Orthogonalize(mu);
  const int kD = this->dimensionality;
  //x.carriages[mu] = (double*) realloc(x.carriages[mu], x.modes_sizes[mu] * sizeof(double));
  //double one = 1.0;
  //copy(x.modes_sizes[mu], &one, 0, x.carriages[mu], 1);
  TT y = *this;
  y.Apply_mu(x, mu);
  //y.SVDCompression(eps, cut_rank, mu); // slow and need to change a lot?
  double *left = x.LeftToRightDot(y, mu);   //x.ranks[mu] x this->ranks[mu] * x.ranks[mu]
  double *right = x.RightToLeftDot(y, mu);  //x.ranks[mu+1] x this->ranks[mu+1] * x.ranks[mu+1]
  int lda = x.ranks[mu] * x.ranks[mu];
  int ldb = x.ranks[mu + 1] * x.ranks[mu + 1];
  int size = x.ranks[mu] * this->modes_sizes[mu] * x.ranks[mu + 1];
  double *p = (double*) malloc(lda * this->ranks[mu + 1] * sizeof(double));
  double *v = (double*) malloc(lda * ldb * sizeof(double));
  double *A_mu = (double*) calloc(size * size, sizeof(double));

  int scale = (x.power_scale + y.power_scale) / kD;
  int mod_scale = (x.power_scale + y.power_scale) % kD;
  double factor = pow(2.0, scale);
  double double_factor = (mod_scale > 0) ? 2 * factor : factor / 2;
  mod_scale = abs(mod_scale);
  double tmp_factor = (mu < mod_scale) ? double_factor : factor;  // scale mu-th carriage

  for (int i = 0; i < this->modes_sizes[mu]; ++i){
    if (mu == 0){
      gemm(CblasColMajor, CblasNoTrans, CblasTrans, this->ranks[mu], ldb, this->ranks[mu + 1], tmp_factor,
          this->carriages[mu] + i * this->ranks[mu], this->ranks[mu] * this->modes_sizes[mu],
          right, ldb, 0.0, v, this->ranks[mu]);
    }else if (mu == kD - 1){
      gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, lda, this->ranks[mu + 1], this->ranks[mu], factor,
          left, lda, this->carriages[mu] + i * this->ranks[mu], this->ranks[mu] * this->modes_sizes[mu], 0.0, v, lda);
    }else{
      gemm(CblasColMajor, CblasNoTrans, CblasNoTrans, lda, this->ranks[mu + 1], this->ranks[mu], 1.0,
          left, lda, this->carriages[mu] + i * this->ranks[mu], this->ranks[mu] * this->modes_sizes[mu], 0.0, p, lda);
      gemm(CblasColMajor, CblasNoTrans, CblasTrans, lda, ldb, this->ranks[mu + 1], tmp_factor,
          p, lda, right, ldb, 0.0, v, lda);
    }
    for (int alpha = 0; alpha < x.ranks[mu + 1]; ++alpha)
      for (int row_alpha = 0; row_alpha < x.ranks[mu + 1]; ++row_alpha)
        for (int beta = 0; beta < x.ranks[mu]; ++beta){
          int ind = ((alpha * x.ranks[mu + 1] + row_alpha) * x.ranks[mu] + beta) * x.ranks[mu];
          int col = (alpha * this->modes_sizes[mu] + i) * x.ranks[mu] + beta;
          int row = (row_alpha * this->modes_sizes[mu] + i) * x.ranks[mu];
          copy(x.ranks[mu], v + ind, 1, A_mu + size * col + row, 1);
      }
  }
  free(left);
  free(right);
  free(p);
  free(v);
  return A_mu;
}

void TT::Print(int mu, int carriages) const{
  std::cout << "dimension " << this->dimensionality << '\n';
  std::cout << "orthogonality " << this->orthogonality << '\n';
  std::cout << "power_scale " << this->power_scale << '\n';
  std::cout << "max rank " << this->max_rank << '\n';
  std::cout << "max carriages size " << this->max_carriages_size << '\n';
  for (int k = 0; k < this->dimensionality; ++k){
    if (k == mu) continue;
    std::cout << "tt " << k << "-th carriage: " << this->ranks[k] << " * " << this->modes_sizes[k] << " x " << this->ranks[k + 1] << '\n';
    if (!carriages) continue;
    for (int j = 0; j < this->modes_sizes[k]; ++j)
      for (int l = 0; l < this->ranks[k]; ++l)
        for (int i = 0; i < this->ranks[k + 1]; ++i){
        int ind = i * this->modes_sizes[k] * this->ranks[k] + j * this->ranks[k] + l;
        std::cout << this->carriages[k][ind] << " \n"[i == (this->ranks[k + 1] - 1)];
      }
  }
}
