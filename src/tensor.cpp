#include "../include/tensor.h"

TTensor::TTensor(const int d, const int *m):
  dimensionality(d),
  modes_sizes(NULL){
  modes_sizes = (int *) malloc(d * sizeof(int));
  copy(d, m, 1, modes_sizes, 1);
}

TTensor::TTensor(const int d, int *m):
  dimensionality(d),
  modes_sizes(NULL){
  modes_sizes = (int *) malloc(d * sizeof(int));
  copy(d, m, 1, modes_sizes, 1);
}

TTensor::TTensor():
  dimensionality(0),
  modes_sizes(NULL){
}

TTensor::TTensor(const TTensor &tt):
  dimensionality(tt.dimensionality),
  modes_sizes(nullptr){
    modes_sizes = (int *) malloc(dimensionality * sizeof(int));
    copy(dimensionality, tt.modes_sizes, 1, modes_sizes, 1);
}

TTensor::~TTensor(){
  dimensionality = 0;
  free(modes_sizes);
  modes_sizes = NULL;
}

int TTensor::GetDimensionality() const{
  return dimensionality;
}

int TTensor::GetModeSize(const int &i) const{
  if (i < dimensionality){
    try{
        return modes_sizes[i];
    }
    catch(...){
        return 0;
    }
  }
  else return 0;
}

int TTensor::GetUnfoldingMatrixRowsNumber(const int &i) const{
  if (i > dimensionality) return 0;
  int rows_number = 1, j;
  for (j = 0; j < i; ++j) rows_number *= modes_sizes[j];
  return rows_number;
}

int TTensor::GetUnfoldingMatrixColumnsNumber(const int &i) const{
  if (i > dimensionality) return 0;
  int columns_number = 1, j;
  for (j = i; j < dimensionality; ++j) columns_number *= modes_sizes[j];
  return columns_number;
}

TUnfoldingSubMatrix::TUnfoldingSubMatrix(TTensor* tens, int k1, int m, int n, int **r, int **c):
  TMatrix(m, n),
  tensor(tens),
  k(k1),
  row_indeces(r),
  column_indeces(c){
}

TUnfoldingSubMatrix::~TUnfoldingSubMatrix(){
  tensor = NULL;
  row_indeces = NULL;
  column_indeces = NULL;
}

double TUnfoldingSubMatrix::Value(const int &i, const int &j){
  const int kD = this->tensor->GetDimensionality();
  int *index = (int*) malloc(kD * sizeof(int));
  for (int o = 0; o < k; ++o) index[o] = row_indeces[i][o];
  for (int o = k; o < kD; ++o)
    index[o] = column_indeces[j][kD - o - 1];
  double result = (*this->tensor)[index];
  free(index);
  return result;
}
