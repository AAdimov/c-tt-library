.SUFFIXES:
.SUFFIXES: .cpp .o

include Makefile.in

DEPS    =  src/blas_double.cpp src/blas_int.cpp \
					 src/cross.cpp src/matrix.cpp \
					 src/tensor.cpp src/tensor_train.cpp \
					 src/maxvol.cpp  src/parallel_cross_omp.cpp \
					 src/tt.cpp src/solvers.cpp \
					 src/eigensolvers.cpp \
					 src/blocktt.cpp

OBJS    = $(DEPS:.cpp=.o)

.cpp.o:
	$(CXX) -c $(COPT) -o $@  $<

lib: $(OBJS)
	$(AR) rc $(LIBNAME) $(OBJS)
	ranlib $(LIBNAME)

test:
	$(CXX) tests/TT_TESTS.cpp $(COPT) libtt.a -o TEST.out $(LIBS) $(LIBS) $(LIBS)

max:
	$(CXX) tests/MAX.cpp $(COPT) libtt.a -o MAX.out $(LIBS) $(LIBS) $(LIBS)

speedtest:
	$(CXX) tests/Speed_Tests.cpp $(COPT) libtt.a -o SPEEDTEST.out $(LIBS) $(LIBS) $(LIBS)

documentation:
	cd doc/ && doxygen Doxyfile

clean:
	rm -f $(OBJS) libtt.a
	rm -f EXAMPLE.out
	rm -f TEST.out
	rm -f MAX.out
	rm -f SPEEDTEST.out

cleanobj:
	rm -f $(OBJS)
	rm -f EXAMPLE.out
	rm -f TEST.out
	rm -f MAX.out
	rm -f SPEEDTEST.out

cleandoc:
	rm -rf doc/html doc/latex
