#include "../include/tt.h"
#include "../include/solvers.h"
#include "../include/eigensolvers.h"
#include "../include/blocktt.h"
#include <set>

// Function 1
class TTest1: public TTensor{  // ./MAX.out 50 3 1 1 1 1 failed cross approximation
	public:
                // f = (f_1, f_2, ..., f_d)
                // f_i(x_1, x_2, ... ,x_d) = (x_1^3 + x_2^3 + ...+ e^(x_i - 1) +... + x_d^3 - d
                // x_i at uniform mesh in [0, 1]^d
                // -||f||^2
		double operator[](const int *point) const{
      double res = 0.0;
      const int kD = this->GetDimensionality();
      double s0 = 0.0;
      double s1 = 0.0;
      double s2 = 0.0;
      for (int j = 0; j < kD; ++j){
        double x_j = (this->GetModeSize(j) == 1) ? point[j] : point[j] * 1.0 / (this->GetModeSize(j) - 1.0);
        double a = (2*x_j * 2*x_j * 2*x_j);
        double b = exp(x_j - 0.5) - a;
        s0 += a;
        s1 += b;
        s2 += b * b;
      }
      res = kD * (s0 - kD) * (s0 - kD) + 2 * (s0 - kD) * s1 + s2;
      return -res;
    }
		TTest1(): TTensor(){
		}
		TTest1(const int d, int *m): TTensor(d, m){
		}
};

// Function 2
class TTest2: public TTensor{
	public:
                // f(x_1, x_2, ... ,x_d) = 1 // 10.0 + e^{-(x_1 + x_2 + ... + x_d)}
                // x_i at uniform mesh in [0,1]^d
		double operator[](const int *point) const{
			//double s = 0;
			//for (int i = 0; i < this->GetDimensionality(); ++i){
			//	s += point[i] * (1.0 / (this->GetModeSize(i)));
			//}
			return 1;
		}
		TTest2():TTensor(){
		}
		TTest2(const int d, int *m): TTensor(d, m){
		}
};

// Function 3
class TTest3: public TTensor{
	public:
                // f(x_1, x_2, ... ,x_d) = 10.0 +(x_1 + x_2 + ... + x_d
                // x_i at uniform mesh in [0,1]^d
		double operator[](const int *point) const{
			double s = 0.0;
			for (int i = 0; i < this->GetDimensionality(); ++i)
				s += point[i] * 1.0 / (this->GetModeSize(i) - 1.0);
			return 10.0 + s;
		}
		TTest3(): TTensor(){
		}
		TTest3(const int d, int *m): TTensor(d, m){
		}
};


// Function 4
class TTest4: public TTensor{
	public:
		double operator[](const int *point) const{
      double p = 1.0;
      for (int j = 0; j < this->GetDimensionality(); ++j){
        double x_j = (this->GetModeSize(j) == 1) ? point[j] : point[j] * 1.0 / (this->GetModeSize(j) - 1.0);
        p *= x_j;
      }
			return p;
		}
		TTest4(): TTensor(){
		}
		TTest4(const int d, int *m): TTensor(d, m){
		}
};

// Function 5
class TTest5: public TTensor{  //./MAX.out 4 11 1 100 1 1
	public:
                // f = (f_1, f_2, ..., f_d)
                // f_i(x_1, x_2, ... ,x_d) = (x_1^3 + x_2^3 + ...+ e^(x_i - 1) +... + x_d^3 - d
                // x_i at uniform mesh in [0, 1]^d
                // 1 / (1e-6 +||f||^2)
		double operator[](const int *point) const{
			double res = 0.0;
      const int kD = this->GetDimensionality();
      double s0 = 0.0;
      double s1 = 0.0;
      double s2 = 0.0;
      for (int j = 0; j < kD; ++j){
        double x_j = point[j] * 1.0 / (this->GetModeSize(j) - 1.0);
        double a = (2*x_j * 2*x_j * 2*x_j);
        double b = exp(x_j - 0.5) - a;
        s0 += a;
        s1 += b;
        s2 += b * b;
      }
      res = (s0 - kD) * (s0 - kD) * kD + 2 * (s0 - kD) * s1 + s2;
			return (1.0 / (1e-6 + res));
		}
		TTest5(): TTensor(){
		}
		TTest5(const int d, int *m): TTensor(d, m){
		}
};

void Print(TT &tt1, int d, int N, unsigned int B){
  std::cout.precision(16);
  int *index = (int*) malloc(d * sizeof(int));
  for (int k = 0; k < d; ++k)
    index[k] = 0;
  std::multiset<double> mx;
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j){
      index[0] = i;
      index[1] = j;
      if (d >= 3)
        for (int l = 0; l < N; ++l){
          index[2] = l;
          if (d >= 4)
            for (int w = 0; w < N; ++w){
              index[3] = w;
              if (mx.size() < B)
                mx.insert(tt1[index]);
              else if (tt1[index] > (*mx.begin())){
                mx.erase(mx.begin());
                mx.insert(tt1[index]);
              }
              if (tt1[index] < 0)
              std::cout << tt1[index] << ' ' << w << ' ' << l << ' ' << i << ' ' <<j << '\n';
            }
          else{
            if (mx.size() < B)
              mx.insert(tt1[index]);
            else if (tt1[index] > (*mx.begin())){
              mx.erase(mx.begin());
              mx.insert(tt1[index]);
            }
          }
          //std::cout << tt1[index] << ' ';
        }
      else{
        if (mx.size() < B)
          mx.insert(tt1[index]);
        else if (tt1[index] > (*mx.begin())){
          mx.erase(mx.begin());
          mx.insert(tt1[index]);
        }
      }
    }
  for (auto it = mx.begin(); it != mx.end(); it++)
    std::cout << *it << ' ';
  std::cout << '\n';
  free(index);
}

int main(int argc, char **argv){
  omp_set_num_threads(1);
  srand(239);
  int i, j, N, d, *modes_sizes1, *modes_sizes2;
  unsigned niters = 100, niters2 = 10, als_iters = 1, starts = 1;
  double tol = 1e-6;
  int kNumOfEigenvalues = 3;
  if (argc < 3){
    std::cout << "args: N d" << '\n';
    return 1;
  }
  sscanf(argv[1], "%d", &N);                       // N is mode size along axis
  sscanf(argv[2], "%d", &d);                       // dimensionality of the function
  if (argc > 3)
      sscanf(argv[3], "%d", &kNumOfEigenvalues);
  if (argc > 4)
	    sscanf(argv[4], "%u", &niters);
  if (argc > 5)
	    sscanf(argv[5], "%u", &niters2);
  if (argc > 6)
	    sscanf(argv[6], "%u", &als_iters);
  if (argc > 7)
	    sscanf(argv[7], "%lf", &tol);
  if (argc > 8)
	    sscanf(argv[8], "%u", &starts);
  modes_sizes1 = (int *) malloc(d * sizeof(int));  // allocate mode sizes array
  for (int i = 0; i < d; ++i)                      //
    modes_sizes1[i] = N;                           // fill the modes
  TTest1 test1(d, modes_sizes1);                   // declare first tensor for approximation
                                                   //
  modes_sizes2 = (int *) malloc(d * sizeof(int));  //
  for (int i = 0; i < d; ++i)                      //
    modes_sizes2[i] = N;                           //
  TTest2 test2(d, modes_sizes2);                   // declare second tensor for approximation
                                                   //
  TTensorTrainParameters parameters;               // parameters structure for TT Cross approximation
  parameters.tolerance = tol;                      // convergence parameter
  parameters.maximal_iterations_number = 0;        // number of iterations. iterates until convergence if set as 0
  parameters.stop_rank = 50;
  parameters.cross_max_iterations = 500;

  TTest5 test5(d, modes_sizes1);                                                   //
  TT tt1, tt2;                                     // declare objects for tensor trains
  std::cout << "Approximation\n";
  double tm_cross = omp_get_wtime();
  tt1.Approximate(&test5, parameters);             // call TT Cross approximation of target function 1
  tt2.Approximate(&test2, parameters);             // call TT Cross approximation of target function 2
  std::cout << "Approximate time: " << omp_get_wtime() - tm_cross << " OK\n";
  //if (d <= 10)
    tt1.Print();
  std::cout << "log norm " << tt1.LogNorm() << '\n';
  double eps = 1e-12;
  const int cut_rank = 5, cut_rank2 = 10;
  TT tt2_copy = tt2, tt2_copy2 = tt2;
  tt1.SVDCompression(eps, 20);
  //if (d <= 10)
    tt1.Print();
  //if ((kNumOfEigenvalues < 50) && (pow(N, 4) < 1e6))
  //  Print(tt1, d, N, kNumOfEigenvalues);
/*  int mu = 1;
  int ind = mu;
  int ind2 = mu + 1;
  int size = tt1.GetRank(ind) * tt1.GetModeSize(ind) * tt1.GetRank(ind2);
  std::cout << size << '\n';
  double *matrix = tt1.MakeLocalSystem(tt2, mu);
  for (int i = 0; i < size; ++i)
    for (int j = 0; j < size; ++j)
      std::cout << matrix[j * size + i] << " \n"[j == size-1];
  free(matrix);
  tt1.Print();
*/
  std::cout.precision(5);
  double tm_als = omp_get_wtime();
  BlockTT btt(tt2_copy2, 0, kNumOfEigenvalues);
  btt.SVDCompression(eps, cut_rank);
  std::cout << "block created\n";
  ALS(tt1, btt, eps, kNumOfEigenvalues * cut_rank2, als_iters);
  for (int b = 0; b < kNumOfEigenvalues; ++b){
    TT max_ev = btt(b);
    double mx_als = RayleighQuotient(tt1, max_ev);
    std::cout << mx_als << '\n';
  }
  std::cout << "ALS 1 time: " << omp_get_wtime() - tm_als << '\n';

  int *ranks1 = (int *) malloc((d + 1) * sizeof(int));
  ranks1[0] = 1;
  for (int i = 1; i < d; ++i) ranks1[i] = 5;
  ranks1[d] = 1;
  for (unsigned int i = 0; i < starts; ++i){
    BlockTT btt(d, modes_sizes1, ranks1, 0, kNumOfEigenvalues);
    btt.SVDCompression(eps, cut_rank);
    std::cout << "block created\n";
    ALS(tt1, btt, eps, kNumOfEigenvalues * cut_rank2, als_iters);
    for (int b = 0; b < kNumOfEigenvalues; ++b){
      TT max_ev = btt(b);
      double mx_als = RayleighQuotient(tt1, max_ev);
      std::cout << mx_als << '\n';
    }
    std::cout << "ALS 2 time: " << omp_get_wtime() - tm_als << '\n';
  }
/*
  ////////////////////////////////////////////////
  double tm = omp_get_wtime();
  TT max_ev = PowerIterations(tt1, tt2, eps, cut_rank, niters);
  double mx = RayleighQuotient(tt1, max_ev);
  std::cout << mx << '\n';
  std::cout << "Power iterations time: " << omp_get_wtime() - tm << '\n';
*/
/*
  tt2 = tt2_copy;
  ////////////////////////////////////////////////
  double tm1 = omp_get_wtime();
  TT max_ev1;
  max_ev1 = InverseIterations(tt1, tt2, 10 + d * 0.9, eps, cut_rank, niters2, 10, 1);
  double mx1 = RayleighQuotient(tt1, max_ev1);
  std::cout << mx1 << '\n';
  std::cout << "Inverse iterations time: " << omp_get_wtime() - tm1 << '\n';
*/
  ////////////////////////////////////////////////
  TT *tt3 = new TT[kNumOfEigenvalues]();
  for (int i = 0; i < kNumOfEigenvalues; ++i) tt3[i] = TT(d, modes_sizes1, ranks1);

  ////////////////////////////////////////////////
  TT * max_evs;
  //for (int i = 0; i < kNumOfEigenvalues; ++i) tt3[i] = tt2_copy;
  double tm_bpi = omp_get_wtime();
  max_evs = BlockPowerIterations(tt1, tt3, kNumOfEigenvalues, eps, cut_rank, niters);
  for (int i = 0; i < kNumOfEigenvalues; ++i){
    double mx = RayleighQuotient(tt1, max_evs[i]);
    std::cout << i << ' ' << mx << '\n';
  }
  std::cout << "block power iterations time: " << omp_get_wtime() - tm_bpi << '\n';
  for (int i = 0; i < kNumOfEigenvalues; ++i)
    for (int j = 0; j < kNumOfEigenvalues; ++j)
      std::cout << max_evs[i].Dot(max_evs[j]) << ' ';
  std::cout << '\n';

  ////////////////////////////////////////////////
  for (int i = 0; i < kNumOfEigenvalues; ++i) tt3[i] = TT(d, modes_sizes1, ranks1);
  tt3 = BlockPowerIterations(tt1, tt3, kNumOfEigenvalues, eps, cut_rank, 1);
  for (int i = 0; i < kNumOfEigenvalues; ++i){
    double mx = RayleighQuotient(tt1, tt3[i]);
    std::cout << i << ' ' << mx << '\n';
  }
  double *shift = new double[kNumOfEigenvalues];
  for (int i = 0; i < 1; ++i)
    shift[i] = 1000000;
  for (int i = 1; i <= std::min(kNumOfEigenvalues, d); ++i)
    shift[i] = i * 0.1;
  //for (int i = d + 1; i <= kNumOfEigenvalues; ++i)
  //  shift[i] -= 0.2;
  double tm = omp_get_wtime();
  max_evs = InverseIterations(tt1, tt3, kNumOfEigenvalues, shift, eps, cut_rank, niters2, 3, 2);
  for (int i = 0; i < kNumOfEigenvalues; ++i){
    double mx = RayleighQuotient(tt1, max_evs[i]);
    std::cout << i << ' ' << mx << '\n';
  }
  std::cout << "Inverse iterations time: " << omp_get_wtime() - tm << '\n';

  delete [] shift;
  delete [] tt3;
  free(modes_sizes1);
  free(modes_sizes2);
  return 0;
}
