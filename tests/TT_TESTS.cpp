#include "../include/tt.h"
#include "../include/solvers.h"
#include "../include/eigensolvers.h"
#include <iomanip>

class TTest1: public TTensor{
	public:
                // f(x_1, x_2, ... ,x_d) = 10.0 +(x_1 + x_2 + ... + x_d
                // x_i at uniform mesh in [0,1]^d
		double operator[](const int *point) const{
			double s = 0;
			for (int i = 0; i < this->GetDimensionality(); ++i)
				s += point[i] * (1.0 / (this->GetModeSize(i)));
			return 10.0 + s;
		}
		TTest1(): TTensor(){
		}
		TTest1(const int d, int *m): TTensor(d, m){
		}
};

void Print(TT &tt1, TT &tt2, int d, int N){
  std::cout << "operator[]\n";
  int *index = (int*) malloc(d * sizeof(int));
  for (int k = 0; k < d; ++k)
    index[k] = 0;
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j){
      index[0] = i;
      index[1] = j;
      std::cout << tt2[index] - tt1[index] << ' ';
    }
  std::cout << '\n';
  free(index);
}

void Print(TT &tt1, int d, int N){
  std::cout << "operator[]\n";
  int *index = (int*) malloc(d * sizeof(int));
  for (int k = 0; k < d; ++k)
    index[k] = 0;
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      for (int l = 0; l < N; ++l){
        index[0] = i;
        index[1] = j;
        index[2] = l;
        std::cout << tt1[index] << ' ';
    }
  std::cout << '\n';
  free(index);
}

TT Multiplication(const TT &A, const TT &x, double eps, const int cut_rank){
  TT C = A * x;
  C.SVDCompression(eps, cut_rank);
  return C;
}

int main(int argc, char **argv){
  std::cout << std::setprecision(2);
  const int d = 3, N = 4;
  int *modes_sizes1, *modes_sizes2;
  TTensorTrainParameters parameters;

  modes_sizes1 = (int *) malloc(d * sizeof(int));  // allocate mode sizes
  modes_sizes2 = (int *) malloc(d * sizeof(int));
  for (int i = 0; i < d; ++i){                     // fill the modes
		modes_sizes1[i] = N;
    modes_sizes2[i] = N;
  }

  // Create TT
  int *ranks1 = (int *) malloc((d + 1) * sizeof(int));
  ranks1[0] = 1;
  ranks1[1] = 2;
  ranks1[2] = 3;
  ranks1[d] = 1;
  double **carriages1 = (double **) malloc(d * sizeof(double*));
  for (int k = 0; k < d; ++k){
    int size = ranks1[k] * modes_sizes1[k] * ranks1[k + 1];
    carriages1[k] = (double *) malloc(size * sizeof(double));
    for (int i = 0; i < ranks1[k + 1]; ++i)
      for (int j = 0; j < modes_sizes1[k]; ++j)
        for (int l = 0; l < ranks1[k]; ++l){
          int ind = i * modes_sizes1[k] * ranks1[k] + j * ranks1[k] + l;
          carriages1[k][ind] = 1.0 * (ind + 1);
        }
  }

  // Create TT2
  int *ranks2 = (int *) malloc((d + 1) * sizeof(int));
  ranks2[0] = 1;
  ranks2[1] = 3;
  ranks2[2] = 2;
  ranks2[d] = 1;
  double **carriages2 = (double **) malloc(d * sizeof(double*));
  for (int k = 0; k < d; ++k){
    int size = ranks2[k] * modes_sizes1[k] * ranks2[k + 1];
    carriages2[k] = (double *) malloc(size * sizeof(double));
    for (int i = 0; i < ranks2[k + 1]; ++i)
      for (int j = 0; j < modes_sizes1[k]; ++j)
        for (int l = 0; l < ranks2[k]; ++l){
          int ind = i * modes_sizes1[k] * ranks2[k] + j * ranks2[k] + l;
          carriages2[k][ind] = 1.0 * (ind + 3);
        }
  }
  std::cout << "TT() constructor" << '\n';
  TT tt;

  std::cout << "TT(int, int* &, int*, double**) constructor\n";
  TT tt_1(d, modes_sizes1, ranks1, carriages1);
  TT tt_2(d, modes_sizes1, ranks2, carriages2);
  std::cout << "tt1 has been created\n";
  //tt1.Print();
  std::cout << "TT(const TT&) constructor\n";
  TT tt1 = tt_1;
  TT tt2 = tt1;

  std::cout << "TT(tt, 1.0) constructor" << '\n';
  TT I = TT(tt2, 1.0);
  std::cout << I.LogNorm() << " sqrt(N^d)" << '\n';

  double * left  = tt_1.LeftToRightDot(tt_2, d);
  double * right = tt_1.RightToLeftDot(tt_2, -1);
  std::cout << sqrt(tt_1.Dot(tt_2)) << ' ' << sqrt(left[0]) << ' ' << sqrt(right[0]) << '\n';
  free(left);
  free(right);

  std::cout << "////////////////////////\n";
  std::cout << "TT operator=\n";
  tt2 = tt1;

  std::cout << "////////////////////////\n";
  std::cout << "TT operator*=(double alpha)\n";
  tt1 *= 3.0;
  //std::cout << "tt1\n";
  //tt1.Print();

  std::cout << "////////////////////////\n";
  std::cout << "TT operator-\n";
  tt2 = tt2 + tt2;
  tt2 = tt1 - tt2;
  //Print(tt1, tt2, d, N);

  std::cout << "////////////////////////\n";
  std::cout << "TT operator+=\n";
  tt2 += tt2 + tt2;
  std::cout << "tt2\n";

  std::cout << "DOT " << sqrt(tt1.Dot(tt2)) << '\n';
  //tt2.Print();

  //tt2.LeftToRightOrthogonalization();
  //tt2.RightToLeftOrthogonalization();

  //Print(tt1, tt2, d, N);

  std::cout << "////////////////////////\n";
  std::cout << "SVDCompression cut_rank = 2\n";
  tt2.SVDCompression(1e-12, 2);
  std::cout << "tt2\n";
  //tt2.Print();

  Print(tt1, tt2, d, N);

  std::cout << "////////////////////////\n";
  std::cout << "TT Approximate of tt1\n";
  parameters.tolerance = 1e-6;
  parameters.maximal_iterations_number = 0;
  //std::cout << "tt1\n";
  //tt1.Print();
  TT tt3;
  tt3.Approximate(&tt1, parameters);
  //tt3.Print();

  //Print(tt1, tt3, d, N);

  std::cout << "////////////////////////\n";
  std::cout << "TT Approximate of test1\n";
  TT tt4;
  TTest1 test1(d, modes_sizes2);
  parameters.tolerance = 1e-6;
  parameters.maximal_iterations_number = 0;
  tt4.Approximate(&test1, parameters);
  //std::cout << "tt4\n";
  //tt4.Print();

  std::cout << "////////////////////////\n";
  std::cout << "TT::Dot\n";
  std::cout << sqrt(tt2.Dot(tt2)) << ' ' << pow(2, tt2.LogNorm()) << '\n';
  tt2.Normalize();
  std::cout << sqrt(tt2.Dot(tt2)) << '\n';


  std::cout << "////////////////////////\n";
  std::cout << "TT-GMRES\n";
  TT A, x, b;
  double **Acarriages = (double **) malloc(d * sizeof(double*));
  double **bcarriages = (double **) malloc(d * sizeof(double*));
  double **xcarriages = (double **) malloc(d * sizeof(double*));
  for (int k = 0; k < d; ++k){
    int size = ranks1[k] * modes_sizes1[k] * ranks1[k + 1];
    Acarriages[k] = (double *) malloc(size * sizeof(double));
    bcarriages[k] = (double *) malloc(size * sizeof(double));
    xcarriages[k] = (double *) malloc(size * sizeof(double));
    for (int i = 0; i < ranks1[k + 1]; ++i)
      for (int j = 0; j < modes_sizes1[k]; ++j)
        for (int l = 0; l < ranks1[k]; ++l){
          int ind = i * modes_sizes1[k] * ranks1[k] + j * ranks1[k] + l;
          Acarriages[k][ind] = 1.0 * (ind + 3);
          bcarriages[k][ind] = 1.0 * (i != 0) * (j != 0);
          xcarriages[k][ind] = 1.0 * (ind + 1);
        }
  }
  A = TT(d, modes_sizes1, ranks1, Acarriages);
  x = TT(d, modes_sizes1, ranks1, xcarriages);
  //TT y = x;
  b = TT(d, modes_sizes1, ranks1, bcarriages);

  /*TTGMRES(A, x, b, Multiplication, 1e-12, 10, 10, 5);
  TT C = A * x;
  Print(C, b, d, N);
*/
  x.Print();
  for (int mu = 0; mu < d; ++mu){
    x.Orthogonalize(mu);
    double *left = x.LeftToRightDot(x, mu);
    double *right = x.RightToLeftDot(x, mu);
    std::cout << "left\n";
    if (mu != 0){
      for (int i = 0; i < x.GetRank(mu); ++i)
        for (int j = 0; j < x.GetRank(mu); ++j)
          std::cout << left[j * x.GetRank(mu) + i] << " \n"[j==x.GetRank(mu)-1];
    }
    std::cout << "right\n";
    if (mu != d - 1){
      int q = mu + 1;
      for (int i = 0; i < x.GetRank(q); ++i)
        for (int j = 0; j < x.GetRank(q); ++j)
          std::cout << right[j * x.GetRank(q) + i] << " \n"[j==x.GetRank(q)-1];
    }
  }

  int mu = 1;
  int ind = mu;
  int ind2 = mu + 1;
  int size = x.GetRank(ind) * x.GetModeSize(ind) * x.GetRank(ind2);
  std::cout << size << ' ' << x.GetRank(ind) << ' ' << x.GetRank(ind2) << '\n';
  double *matrix = A.MakeLocalSystem(x, mu, 0.0, 1);
  for (int i = 0; i < size; ++i)
    for (int j = 0; j < size; ++j){
      if (fabs(matrix[j * size + i]) < 0.0001)
        std::cout << 0 << " \n"[j == size-1];
      else
        std::cout << matrix[j * size + i] << " \n"[j == size-1];
    }
  for (int k = 0; k < d; ++k){
    free(carriages1[k]);
    free(carriages2[k]);
    free(Acarriages[k]);
    free(bcarriages[k]);
    free(xcarriages[k]);
  }
  free(Acarriages);
  free(xcarriages);
  free(bcarriages);
  free(carriages1);
  free(carriages2);
  free(modes_sizes1);
  free(modes_sizes2);
  free(ranks1);
  free(ranks2);
  free(matrix);
  return 0;
}
