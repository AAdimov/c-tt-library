#include "../include/tt.h"
#include "../include/solvers.h"
#include "../include/eigensolvers.h"
#include "../include/blocktt.h"

void TT_vs_TTensorTrain(){
  int D = 32;
  int N = 8;
  int R = 64;
  double total_time1 = 0;
  double total_time2 = 0;
  for (int d = 2; d <= D; d *= 4){
    std::cerr << d << '\n';
    int *modes_sizes = (int *) malloc(d * sizeof(int));
    int *ranks = (int *) malloc((d + 1) * sizeof(int));
    int *index = (int *) calloc(d, sizeof(int));
    ranks[d] = 1;
    for (int n = 2; n <= N; n *= 2){
      std::cerr << n << '\n';
      copy(d, &n, 0, modes_sizes, 1);
      for (int r = 1; r <= R; r *= 2){
        copy(d, &r, 0, ranks, 1);
        ranks[0] = 1;
        double time1 = 0;
        double time2 = 0;
        for (int i = 0; i < 100; ++i){
          TT a_1 = TT(d, modes_sizes, ranks);
          TT b_1 = TT(d, modes_sizes, ranks);
          TTensorTrain a_2 = a_1.ConvertToTTensorTrain();
          TTensorTrain b_2 = b_1.ConvertToTTensorTrain();
          double tm1 = omp_get_wtime();
/////////////////////////////////////////////////
          //a_1[index];
          //a_1 = b_1;
          //a_1.LeftToRightOrthogonalization();
          //a_1.SVDCompression(1e-12, 10);
          //a_1 + b_1;
          //TT c_1 = a_1 + b_1;
          //(a_1 * b_1).SVDCompression(1e-12, 50);
          //a_1.Dot(b_1);
          //a_1.Dot(a_1);
          //a_1.Nrm2();
          TT c_1 = a_1;
          //TTensorTrain c_2 = a_1.ConvertToTTensorTrain();
          //TT c_1 = c_1.ConvertFromTTensorTrain(a_2);
          time1 += omp_get_wtime() - tm1;
          double tm2 = omp_get_wtime();
/////////////////////////////////////////////////
          //a_2[index];
          //a_2 = b_2;
          //a_2.SVDCompress(1e-12, 10);
          //a_2 + b_2;
          //TTensorTrain c_2 = a_2 + b_2;
          //a_2 * b_2;
          //a_2.Dot(b_2);
          //a_2.Dot(a_2);
          //a_2.Nrm2();
          //TTensorTrain c_2 = a_2; // doesn't work, copy constructor not implemented
          time2 += omp_get_wtime() - tm2;
        }
        total_time1 += time1;
        total_time2 += time2;
        std::cout << d << ' ' << n << ' ' << r << ' ' << time2 << ' ' << time1  << '\n';
      }
    }
    free(index);
    free(ranks);
    free(modes_sizes);
  }
  std::cout << total_time2 << ' ' << total_time1 << '\n';
}

int main(int argc, char **argv){
  omp_set_num_threads(1);
  srand(239);
  freopen("output.txt", "w", stdout);
  TT_vs_TTensorTrain();
  return 0;
}
