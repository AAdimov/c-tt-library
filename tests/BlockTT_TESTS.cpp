#include "../include/blocktt.h"

// Function 6
class TTest6: public TTensor{  //./MAX.out 4 11 1 100 1 1
	public:
                // f = (f_1, f_2, ..., f_d)
                // f_i(x_1, x_2, ... ,x_d) = (x_1^3 + x_2^3 + ...+ e^(x_i - 1) +... + x_d^3 - d
                // x_i at uniform mesh in [0, 1]^d
                // 1 / (1e-6 +||f||^2)
		double operator[](const int *point) const{
			double res = 0.0;
      const int kD = this->GetDimensionality();
      double s0 = 0.0;
      double s1 = 0.0;
      double s2 = 0.0;
      for (int j = 0; j < kD; ++j){
        double x_j = point[j] * 1.0 / (this->GetModeSize(j) - 1.0);
        double a = (2*x_j * 2*x_j * 2*x_j);
        double b = exp(x_j - 0.5) - a;
        s0 += a;
        s1 += b;
        s2 += b * b;
      }
      res = (s0 - kD) * (s0 - kD) * kD + 2 * (s0 - kD) * s1 + s2;
			return (1.0 / (1e-6 + res));
		}
		TTest6(): TTensor(){
		}
		TTest6(const int d, int *m): TTensor(d, m){
		}
};


int main(int argc, char **argv){
  int i, j, N, d, *modes_sizes1, *modes_sizes2;
  unsigned niters = 100, niters2 = 10;
  int kNumOfEigenvalues = 3;
  if (argc < 3){
    std::cout << "args: N d" << '\n';
    return 1;
  }
  sscanf(argv[1], "%d", &N);                       // N is mode size along axis
  sscanf(argv[2], "%d", &d);                       // dimensionality of the function
  if (argc > 3)
      sscanf(argv[3], "%d", &kNumOfEigenvalues);
  if (argc > 4)
	    sscanf(argv[4], "%u", &niters);
  if (argc > 5)
	    sscanf(argv[5], "%u", &niters2);
  modes_sizes1 = (int *) malloc(d * sizeof(int));  // allocate mode sizes array
  for (int i = 0; i < d; ++i)                      //
    modes_sizes1[i] = N;                           // fill the modes

  TTest6 test6(d, modes_sizes1);

  TTensorTrainParameters parameters;               // parameters structure for TT Cross approximation
  parameters.tolerance = tol;                      // convergence parameter
  parameters.maximal_iterations_number = 0;        // number of iterations. iterates until convergence if set as 0


  TT tt1, tt2;
  tt1.Approximate(&test6, parameters);  
  return 0;
}
